# Setup

1. add whyperf dir to scan path;

config -> autiload -> annotations.php

add 

'scan' => [
        'paths' => [
            ...,
            \Whyperf\Whyperf::getPath(),
            ...,
        ]
]

2. modify bin/hyperf

(function () {
\Whyperf\Whyperf::getInstance()->run();
})();


# Installation using Composer

The easiest way to create a new Hyperf project is to use Composer. If you don't have it already installed, then please install as per the documentation.

To create your new Hyperf project:

$ composer create-project hyperf/hyperf-skeleton path/to/install

Once installed, you can run the server immediately using the command below.

$ cd path/to/install
$ php bin/hyperf.php start

This will start the cli-server on port `9501`, and bind it to all network interfaces. You can then visit the site at `http://localhost:9501/`

which will bring up Hyperf default home page.
