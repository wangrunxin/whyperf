<?php
namespace Whyperf\Exception\Traits;

/**
 * Trait ErrorDecoderTrait
 * @package whyperf\Exception\Traits
 */
trait ErrorDecoderTrait
{

    static function makeTrace()
    {
        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 6);
        foreach ($trace as $key => $line) {
            $trace[$key] = self::fixLine($line);
        }
        return $trace;
    }

    static function fixLine($line)
    {
        $list = [
            "file",
            "line",
            "class",
            "function"
        ];

        foreach ($list as $key) {
            if (! isset($line[$key])) {
                $line[$key] = "unknown";
            }
        }

        return $line;
    }

    static function bref(\Throwable $throwable)
    {
        return $throwable->getMessage() . "\n at line " . $throwable->getLine() . " of file " . $throwable->getFile();
    }

    static function traceToString($line, $key)
    {
        $line = self::fixLine($line);

        return "[{$key}] {$line["class"]}->{$line["function"]}() \nat line {$line["line"]} of file '{$line["file"]}'";
    }

    static function traceToConsoleString($line, $key)
    {
        $line = self::fixLine($line);

        return "<bg=yellow;options=bold>[{$key}]</> {$line["class"]}->{$line["function"]}() <info>at line {$line["line"]} of file '{$line["file"]}</info>'";
    }

    static function traceClassToConsoleString($line, $key)
    {
        $line = self::fixLine($line);

        return "[{$key}] {$line["class"]}->{$line["function"]}()";
    }

    static function traceLocationToConsoleString($line, $key)
    {
        $line = self::fixLine($line);

        return "at line {$line["line"]} of file '{$line["file"]}'";
    }
}
?>