<?php

namespace  Whyperf\Exception\Aspect;

use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\Dispatcher\AbstractRequestHandler;
use Hyperf\HttpServer\Exception\Handler\HttpExceptionHandler;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Whyperf\System\CoroutineEnv\CoreGo;
use Whyperf\Whyperf;

/**
 * @Aspect(
 *   classes={
 *     "Hyperf\HttpServer\Server::getDefaultExceptionHandler",
 *   }
 * )
 */
class DefaultHttpErrorHandlerAspect extends AbstractAspect
{
    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        return [
            get_class(Whyperf::getContainer()->get(HttpExceptionHandler::class))
        ];
    }
}
