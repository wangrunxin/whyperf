<?php
declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace Whyperf\Exception\Handler;

use Hyperf\Di\Annotation\Inject;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;
use const true;
use Whyperf\Exception\Traits\ErrorDecoderTrait;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Whyperf\Whyperf;

/**
 * Class BaseExceptionHandler
 * @package whyperf\Exception\Handler
 * @author WANG RUNXIN
 */
class BaseExceptionHandler
{

    /**
     * @\Hyperf\Di\Annotation\Inject()
     * @var ConsoleOutput
     * @author WANG RUNXIN
     */
    private $output;

    use ErrorDecoderTrait;

    function __construct(){
        $this->output = new ConsoleOutput();
    }

    public function handle(Throwable $throwable)
    {
        do {
            $this->renderThrowable($throwable);
            $this->logThrowable($throwable);
        } while ($throwable = $throwable->getPrevious());
    }

    private function renderThrowable(Throwable $throwable)
    {
        $io = new SymfonyStyle(new ArrayInput([]), $this->output);
        $io->error(self::bref($throwable));
        foreach ($throwable->getTrace() as $key => $line) {
            $io->text(self::traceToConsoleString($line, $key));
        }
    }

    private function logThrowable(Throwable $throwable)
    {
        $logger = Whyperf::getLogger(uniqid());
        $logger->error("START", ["------------------------------------------------------------"]);
        $logger->error(self::bref($throwable));
        foreach ($throwable->getTrace() as $key => $line) {
            $logger->error(self::traceToString($line, $key));
        }
        $logger->error("END", ["------------------------------------------------------------"]);
        $logger->close();
    }
}
