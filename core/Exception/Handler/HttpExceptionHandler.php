<?php

declare(strict_types=1);

namespace Whyperf\Exception\Handler;

use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\ExceptionHandler\Formatter\FormatterInterface;
use Hyperf\HttpMessage\Base\Response;
use Hyperf\HttpMessage\Exception\HttpException;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class HttpExceptionHandler extends CoreExceptionHandler
{

    /**
     * render throwable to console
     * @param Throwable $throwable
     */
    protected function renderThrowable(Throwable $throwable)
    {

    }

    protected function responseError(Throwable $throwable, ResponseInterface $response)
    {
        return $response->withHeader('Server', 'Hyperf')
            ->withStatus($throwable->getStatusCode())
            ->withBody(new SwooleStream(json_encode([
                "code" => $throwable->getStatusCode(),
                "message" => Response::getReasonPhraseByCode($throwable->getStatusCode())
            ])));
    }

    /**
     * Determine if the current exception handler should handle the exception,.
     *
     * @return bool
     *              If return true, then this exception handler will handle the exception,
     *              If return false, then delegate to next handler
     */
    public function isValid(Throwable $throwable): bool
    {
        return $throwable instanceof HttpException;
    }
}
