<?php

declare(strict_types=1);

namespace Whyperf\Exception\Handler;

use Whyperf\Audit\Audit;
use Whyperf\Audit\Model\AuditError;
use Whyperf\Tracker\Tracker;

/**
 * Class AppExceptionHandler
 * @package whyperf\Exception\Handler
 * @author WANG RUNXIN
 */
class AppExceptionHandler extends CoreExceptionHandler
{

}
