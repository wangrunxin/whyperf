<?php


declare(strict_types=1);

namespace Whyperf\Exception\Handler;

use Fig\Http\Message\StatusCodeInterface;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LogLevel;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;
use Whyperf\Audit\Audit;
use Whyperf\Audit\Model\AuditError;
use Whyperf\Exception\Traits\ErrorDecoderTrait;
use Whyperf\System\CoroutineEnv\CoreGo;
use Whyperf\Tracker\Tracker;
use Whyperf\Whyperf;

/**
 * Class AppExceptionHandler
 * @package whyperf\Exception\Handler
 * @author WANG RUNXIN
 */
class CoreExceptionHandler extends ExceptionHandler implements StatusCodeInterface
{
    /**
     * @var StdoutLoggerInterface
     */
    protected $logger;

    private $output;

    use ErrorDecoderTrait;

    public function __construct(StdoutLoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->output = new ConsoleOutput();
    }

    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        do {
            try {
                $target = $throwable;

                $this->stopPropagation();
                $this->renderThrowable($throwable);
                $this->logThrowable($throwable);
//                $this->audit($throwable);


                $response = $this->responseError($target, $response);
                CoreGo::getCoreGo()->setResponse($response);

                return $response;
            } catch (\Throwable $r) {
                $this->renderThrowable($r);
                Whyperf::getSimpleLogger("ErrorHandler")->log(
                    LogLevel::ERROR, $r->getMessage(),
                    [
                        "previous_error" => [
                            $throwable->getMessage(),
                            $throwable->getTraceAsString()
                        ],
                        "trace" => $r->getTraceAsString()
                    ]
                );
            }
        } while ($throwable = $throwable->getPrevious());
    }

    /**
     * render throwable to console
     * @param Throwable $throwable
     */
    protected function renderThrowable(Throwable $throwable)
    {
        $io = new SymfonyStyle(new ArrayInput([]), $this->output);
        $io->error(self::bref($throwable));
        foreach ($throwable->getTrace() as $key => $line) {
            $io->text(self::traceToConsoleString($line, $key));
        }
    }

    public function logThrowable(Throwable $throwable)
    {
        $logger = Whyperf::getInstance()->getLogger(Tracker::getInstance()->getTraceId());
        $logger->error("START", ["------------------------------------------------------------"]);
        $logger->error(self::bref($throwable));
        foreach ($throwable->getTrace() as $key => $line) {
            $logger->error(self::traceToString($line, $key));
        }
        $logger->error("END", ["------------------------------------------------------------"]);
        $logger->close();
    }

    protected function responseError(Throwable $throwable, ResponseInterface $response)
    {
        return $response->withHeader('Server', 'Hyperf')
            ->withStatus(self::STATUS_INTERNAL_SERVER_ERROR)
            ->withBody(new SwooleStream('Internal Server Error.'));
    }

    public function isValid(Throwable $throwable): bool
    {
        return true;
    }
}