<?php

namespace Whyperf\Tcc\Amqp;

use Hyperf\Amqp\Message\ProducerMessage;
use Hyperf\Amqp\Message\Type;

class AbstractTccMessageProducer extends ProducerMessage
{
    protected $type = Type::TOPIC;

    public function __construct(string $exchange, string $routingKey, $data)
    {
        $this->exchange = $exchange;
        $this->routingKey = $routingKey;
        $this->payload = $data;
    }

    public function serialize(): string
    {
        return $this->payload;
    }
}
