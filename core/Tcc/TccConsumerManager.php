<?php

namespace Whyperf\Tcc;

use Hyperf\Amqp\Consumer;
use Hyperf\Amqp\ConsumerManager;
use Hyperf\Amqp\Message\ConsumerMessageInterface;
use Hyperf\Process\AbstractProcess;
use Hyperf\Process\ProcessManager;
use Psr\Container\ContainerInterface;
use Whyperf\Whyperf;

class TccConsumerManager extends ConsumerManager
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct()
    {
        $this->container = Whyperf::getContainer();
    }

    public function run()
    {
        $instance = new TccCoordinator();
        property_exists($instance, 'container') && $instance->container = Whyperf::getContainer();
        $nums = $instance->nums;
        $process = $this->createProcess($instance);
        $process->nums = (int)$nums;
        $process->name = $instance->getName() . '-' . $instance->getQueue();
        ProcessManager::register($process);
    }

    private function createProcess(ConsumerMessageInterface $consumerMessage): AbstractProcess
    {
        return new class($this->container, $consumerMessage) extends AbstractProcess {
            /**
             * @var \Hyperf\Amqp\Consumer
             */
            private $consumer;

            /**
             * @var ConsumerMessageInterface
             */
            private $consumerMessage;

            public function __construct(ContainerInterface $container, ConsumerMessageInterface $consumerMessage)
            {
                parent::__construct($container);
                $this->consumer = $container->get(Consumer::class);
                $this->consumerMessage = $consumerMessage;
            }

            public function handle(): void
            {
                $this->consumer->consume($this->consumerMessage);
            }

            public function getConsumerMessage(): ConsumerMessageInterface
            {
                return $this->consumerMessage;
            }

            public function isEnable($server): bool
            {
                return $this->consumerMessage->isEnable();
            }
        };
    }
}

?>