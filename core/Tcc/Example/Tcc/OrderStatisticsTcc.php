<?php


namespace Whyperf\Tcc\Example\Tcc;


use Whyperf\Tcc\Example\Service\OrderService;
use Whyperf\Tcc\TccOption;

class OrderStatisticsTcc extends TccOption
{

    public function try()
    {
        # 增加订单统计
        $service = new OrderService;
        $service->incOrderStatistics();
    }

    public function confirm()
    {
        sleep(10);
        throw new \Exception("test");
        # 空操作
    }

    public function cancel()
    {
        # 减少订单统计
        $service = new OrderService;
        $service->decOrderStatistics();
    }
}