<?php

namespace Whyperf\Tcc;

use App\App;
use Whyperf\Tcc\Coordinator\AbstractTccCoordinator;

class TccCoordinator extends AbstractTccCoordinator
{
    public $nums = 1;

    function __construct()
    {
        $this->exchange = App::getInstance()->getTccExchange();
        $this->routingKey = App::getInstance()->getTccRoutingKey();
        $this->queue = App::getInstance()->getTccQueue();

        return parent::__construct();
    }

    function getName()
    {
        return "whyperf-tcc-consumer";
    }
}
