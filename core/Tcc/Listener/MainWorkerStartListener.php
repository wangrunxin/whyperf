<?php

declare(strict_types=1);

namespace Whyperf\Tcc\Listener;

use App\App;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Framework\Event\MainWorkerStart;
use Hyperf\Server\Event\MainCoroutineServerStart;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Whyperf\Tcc\TccMessageProducer;
use Whyperf\Whyperf;

/**
 * Class MainWorkerStartListener
 * @package Whyperf\Tcc
 * @author WANG RUNXIN
 * @\Hyperf\Event\Annotation\Listener
 */
class MainWorkerStartListener implements ListenerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(ContainerInterface $container, StdoutLoggerInterface $logger)
    {
        $this->container = $container;
        $this->logger = $logger;
    }

    /**
     * @return string[] returns the events that you want to listen
     */
    public function listen(): array
    {
        return [
            MainWorkerStart::class,
            MainCoroutineServerStart::class,
        ];
    }

    /**
     * Handle the Event when the event is triggered, all listeners will
     * complete before the event is returned to the EventDispatcher.
     */
    public function process(object $event)
    {
        $app = App::getInstance();
        if (!$app->enableTcc()) {
            return;
        }
        $producer = Whyperf::getContainer()->get(\Hyperf\Amqp\Producer::class);
        $instance = new TccMessageProducer($app->getTccExchange(), $app->getTccRoutingKey(), []);
        $producer->declare($instance);
        $routingKey = $instance->getRoutingKey();
        if (is_array($routingKey)) {
            $routingKey = implode(',', $routingKey);
        }

        $this->logger->debug(sprintf('AMQP exchange[%s] and routingKey[%s] were created successfully.', $instance->getExchange(), $routingKey));
    }
}
