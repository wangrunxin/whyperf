<?php

declare(strict_types=1);

namespace Whyperf\Tcc\Listener;

use App\App;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Framework\Event\BeforeMainServerStart;
use Hyperf\Server\Event\MainCoroutineServerStart;
use Psr\Container\ContainerInterface;
use Whyperf\Tcc\TccConsumerManager;
use Whyperf\Whyperf;

/**
 * Class BeforeMainServerStartListener
 * @package Whyperf\Tcc
 * @author WANG RUNXIN
 * @\Hyperf\Event\Annotation\Listener
 */
class BeforeMainServerStartListener implements ListenerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return string[] returns the events that you want to listen
     */
    public function listen(): array
    {
        return [
            BeforeMainServerStart::class,
            MainCoroutineServerStart::class,
        ];
    }

    /**
     * Handle the Event when the event is triggered, all listeners will
     * complete before the event is returned to the EventDispatcher.
     */
    public function process(object $event)
    {
        if (!App::getInstance()->enableTcc()) {
            return;
        }
        $consumerManager = Whyperf::getContainer()->get(TccConsumerManager::class);
        $consumerManager->run();
    }
}
