<?php
namespace Whyperf\Tcc\Coordinator;

use Hyperf\Amqp\Message\RpcMessageInterface;
use Hyperf\Amqp\RpcChannel;
use PhpAmqpLib\Message\AMQPMessage;

class TccQueueClient extends \Hyperf\Amqp\RpcClient
{
    public function call(RpcMessageInterface $rpcMessage, int $timeout = 5)
    {
        $pool = $rpcMessage->getPoolName();
        $exchange = $rpcMessage->getExchange();
        $queue = $rpcMessage->getQueueBuilder()->getQueue();

        $chan = $this->initPoolChannel($pool, $exchange, $queue);
        $channel = null;
        try {
            if (!$chan->isEmpty()) {
                $channel = $chan->pop(0.001);
            }

            if (empty($channel)) {
                $connection = $this->factory->getConnection($rpcMessage->getPoolName());
                $channel = new RpcChannel($connection->getChannel());
                $this->initChannel($channel, $rpcMessage->getQueueBuilder());
            }

            $channel->open();

            $message = new AMQPMessage(
                $rpcMessage->serialize(),
                [
                    'correlation_id' => $channel->getCorrelationId(),
                    'reply_to' => $channel->getQueue(),
                ]
            );

            $channel->getChannel()->basic_publish($message, $rpcMessage->getExchange(), $rpcMessage->getRoutingKey());

        } catch (\Throwable $exception) {
            isset($channel) && $channel->close();
            throw $exception;
        }

        $this->release($chan, $channel);
    }
}