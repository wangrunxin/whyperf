<?php

namespace Whyperf\Tcc\Coordinator;

use App\App;
use Hyperf\Amqp\Message\ConsumerMessage;
use Hyperf\Amqp\Message\DynamicRpcMessage;
use Hyperf\Amqp\Producer;
use Hyperf\Amqp\Result;
use Hyperf\Amqp\RpcClient;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Redis\Redis;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Parallel;
use PhpAmqpLib\Message\AMQPMessage;
use Whyperf\Tcc\TccMessageProducer;
use Whyperf\Tcc\Amqp\TccProducer;
use Whyperf\Tcc\Exception\Handle;
use Whyperf\Tcc\Tcc;
use Whyperf\Tcc\TccState;
use Whyperf\Tcc\Util\Di;

class AbstractTccCoordinator extends ConsumerMessage
{
    /**
     * @var Redis
     */
    protected $redis;

    /**
     * @var StdoutLoggerInterface
     */
    protected $logger;

    /**
     * @var Handle
     */
    protected $exception;

    /**
     * @var int
     */
    protected $delay;

    /**
     * @var string
     */
    protected $topic;

    public function __construct()
    {
        $this->redis = Di::redis();
        $this->logger = Di::logger();
        $this->exception = Di::exception();
        $this->delay = 5;
        $this->topic = App::getInstance()->getTccTopic();
    }

    public function consumeMessage($data, AMQPMessage $message): string
    {
        $tccId = (string)$message->getBody();
        $state = $this->getState($tccId);
        $this->logger->info('[TCC Transaction] Begin' . $tccId);

        if ($state instanceof TccState) {
            // 如果事务未处理完毕则延迟检测
            if (!$state->tccStatus) {
                var_dump($state->tccStatus);
                $message = new TccMessageProducer($tccId);
                co(function () use ($message) {
                    sleep(5);
                    $producer = ApplicationContext::getContainer()->get(Producer::class);
                    $producer->produce($message);
                    var_dump($message);
                });
                $this->logger->info('[TCC Transaction] Retried Undone' . $tccId);

                return Result::ACK;
            }

            $this->logger->info('[TCC Transaction] Status ' . $tccId . '#' . $state->optionStep);

            // 如果操作失败则回滚
            if ($state->optionStatus) {
                $this->delState($tccId); // 删除记录
            } else {
                try {
                    // 处理失败的事务并回滚
                    $tcc = new Tcc($tccId, $state);
                    $tcc->runOptionCancel();  // 重试取消
                    $this->delState($tccId);  // 删除记录
                    $this->logger->info('[TCC Transaction] Rollback Success ' . $tccId);
                } catch (\Throwable $e) {
                    $this->pushNotify($tccId, $state, $e);   // 推送通知
                    $this->delState($tccId);                 // 删除记录
                    $this->logger->error('[TCC Transaction] Rollback Failed ' . $tccId);
                }
            }
        } else {
            $this->logger->info('[TCC Transaction] Invalid Status ' . $tccId);
        }

        return Result::ACK;
    }

    protected function pushNotify(string $tccId, TccState $state, \Throwable $e)
    {
        foreach ($state->options as $option) {
            $option->setTcc(null);
        }
        // 调用提供者处理
        $this->exception->handle($tccId, $state, $e);
    }

    protected function delState(string $tccId)
    {
        $this->redis->hDel('tcc', $tccId);
    }

    /**
     * @return TccState|null
     */
    protected function getState(string $tccId)
    {
        $state = (string)$this->redis->hGet('tcc', $tccId);
        if ($state) {
            return unserialize($state);
        }

        return null;
    }
}
