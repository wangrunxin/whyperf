<?php
declare(strict_types = 1);

namespace Whyperf\Constants;

use Hyperf\Constants\AbstractConstants;

/**
 * Class SystemConstants
 * @package whyperf\Constants
 * @author WANG RUNXIN
 */
class SystemConstants extends AbstractConstants
{
    const SERVER_ERROR_HANDLER = 'SERVER_ERROR_HANDLER';
}
