<?php

namespace Whyperf\Database;

use Hyperf\DbConnection\Connection;
use Hyperf\Engine\Coroutine;
use Hyperf\Utils\Context;

class ConnectionManager
{

    protected $ids = [];

    protected $connectionLeakDebug = false;
    protected $includeTrace = false;

    function __construct()
    {
        $this->connectionLeakDebug = config("tenant_database.debug", false);
        $this->includeTrace = config("tenant_database.include_trace", false);
    }

    function addId($id, $name): self
    {
        if ($this->connectionLeakDebug) {
            var_dump(["add" => $id, "coroutine_id" => Coroutine::id(), "pool_name" => $name, "trace" => $this->getTrace()]);
        }

        if (in_array($id, $this->ids)) {
            return $this;
        }
        $this->ids[] = $id;
        return $this;
    }

    function releaseAll(): self
    {
        foreach ($this->ids as $id) {
            $connection = Context::get($id);
            Context::set($id, null);
            if (is_null($connection)) {
                continue;
            }

            if ($connection instanceof Connection || is_subclass_of($connection, Connection::class)) {
                if ($this->connectionLeakDebug) {
                    var_dump(["release" => $id, "coroutine_id" => Coroutine::id(), "trace" => $this->getTrace()]);
                }
                $connection->release();
            }
        }
        $this->ids = [];
        return $this;
    }

    function getTrace()
    {
        return $this->includeTrace ? (new \Exception())->getTraceAsString() : [];
    }

    static function release()
    {
        if (!Context::has(ConnectionManager::class)) {
            $manager = Context::set(ConnectionManager::class, make(ConnectionManager::class));
            return;
        }
        Context::override(ConnectionManager::class, function (ConnectionManager $manager) {
            return $manager->releaseAll();
        });
    }

    static function add($id, $name = null)
    {
        if (!Context::has(ConnectionManager::class)) {
            $manager = Context::set(ConnectionManager::class, make(ConnectionManager::class));
        }

        Context::override(ConnectionManager::class, function (ConnectionManager $manager) use ($id, $name) {
            return $manager->addId($id, $name);
        });
    }
}