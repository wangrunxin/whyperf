<?php

declare(strict_types=1);

namespace Whyperf\Database\Listener;

use Hyperf\Database\Events\QueryExecuted;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Tracer\SpanStarter;
use Hyperf\Tracer\SpanTagManager;
use Hyperf\Tracer\SwitchManager;
use Hyperf\Utils\Arr;
use Hyperf\Utils\Str;
use OpenTracing\Tracer;

/**
 * @\Hyperf\Event\Annotation\Listener
 * Class DatabaseQueryExecutedListener
 * @package Whyperf\Database\Listener
 * @author WANG RUNXIN
 */
class DatabaseQueryExecutedListener implements ListenerInterface
{
    use SpanStarter;

    /**
     * @var Tracer
     */
    private $tracer;

    /**
     * @var SwitchManager
     */
    private $switchManager;

    /**
     * @var SpanTagManager
     */
    private $spanTagManager;

    public function __construct(Tracer $tracer, SwitchManager $switchManager, SpanTagManager $spanTagManager)
    {
        $this->tracer = $tracer;
        $this->switchManager = $switchManager;
        $this->spanTagManager = $spanTagManager;
    }

    public function listen(): array
    {
        return [
            QueryExecuted::class,
        ];
    }

    /**
     * @param QueryExecuted $event
     */
    public function process(object $event)
    {
        if ($this->switchManager->isEnable('database') === false) {
            return;
        }
        $sql = $event->sql;
        if (!Arr::isAssoc($event->bindings)) {
            foreach ($event->bindings as $key => $value) {
                $sql = Str::replaceFirst('?', "'{$value}'", $sql);
            }
        }

        $endTime = microtime(true);
        $span = $this->startSpan('db: ' . $sql, [
            'start_time' => (int)(($endTime - $event->time / 1000) * 1000 * 1000),
        ]);
        $span->setTag($this->spanTagManager->get('database', 'query_time'), $event->time . ' ms');
        $span->setTag($this->spanTagManager->get('database', 'tracing'), print_r(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 10), true));

        $span->finish((int)($endTime * 1000 * 1000));
    }
}
