<?php

namespace Whyperf\Database\Aspect;


use Hyperf\Database\ConnectionInterface;
use Hyperf\Database\Query\Builder;
use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Whyperf\Audit\Audit;
use Whyperf\Audit\Model\AuditData;
use Whyperf\Database\QueryBuilder;
use Whyperf\System\CoroutineEnv\CoreGo;

/**
 * @Aspect(
 *   classes={
 *     "Hyperf\Database\Connection::query",
 *     "Hyperf\Database\Connection::logQuery",
 *   }
 * )
 */
class QueryBuilderAspect extends AbstractAspect
{
    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        switch ($proceedingJoinPoint->getReflectMethod()->getName()) {
            case "query":
                $builder = $proceedingJoinPoint->process();

                return new QueryBuilder(
                    $builder->getConnection(),
                    $builder->getGrammar(),
                    $builder->getProcessor()
                );
                break;
            default:
//                if (!Audit::enabled()) {
//                    return $proceedingJoinPoint->process();
//                }
//                list($sql, $bindings, $time, $result) = $proceedingJoinPoint->getArguments();
//
//                if (Audit::getInstance()->isDisabledSql($sql)) {
//                    return;
//                }

                return $proceedingJoinPoint->process();
        }
    }
}