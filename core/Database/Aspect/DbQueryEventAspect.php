<?php

namespace Whyperf\Database\Aspect;


use Hyperf\Database\ConnectionInterface;
use Hyperf\Database\MySqlConnection;
use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\Tracer\SpanStarter;
use Hyperf\Tracer\SpanTagManager;
use Hyperf\Tracer\SwitchManager;
use OpenTracing\Tracer;
use Whyperf\Audit\Audit;
use Whyperf\Audit\Model\AuditData;
use Whyperf\System\CoroutineEnv\CoreGo;

///**
// * @Aspect(
// *   classes={
// *     "Hyperf\Database\Events\QueryExecuted::__construct",
// *   }
// * )
// */
class DbQueryEventAspect extends AbstractAspect
{

    use SpanStarter;

    /**
     * @var array
     */
    public $annotations = [];

    /**
     * @var Tracer
     */
    private $tracer;

    /**
     * @var SwitchManager
     */
    private $switchManager;

    /**
     * @var SpanTagManager
     */
    private $spanTagManager;


    public function __construct(Tracer $tracer, SwitchManager $switchManager, SpanTagManager $spanTagManager)
    {
        $this->tracer = $tracer;
        $this->switchManager = $switchManager;
        $this->spanTagManager = $spanTagManager;
    }

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        if ($this->switchManager->isEnable('db') === false) {
            return $proceedingJoinPoint->process();
        }

//        return $proceedingJoinPoint->process();

        list($sql, $bindings, $time, $connection, $result) = $proceedingJoinPoint->getArguments();
        /**
         * @var MySqlConnection $connection
         */

        $span = $this->startSpan('db: ' . $sql);
        $span->setTag($this->spanTagManager->get('db', 'statement'), $sql);
        $span->setTag($this->spanTagManager->get('db', 'arguments'), $bindings);
        $span->setTag($this->spanTagManager->get('db', 'query_time'), $time);
        $span->setTag($this->spanTagManager->get('db', 'tracing'), print_r(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 10), true));
        try {
            $result = $proceedingJoinPoint->process();
        } catch (\Throwable $e) {
            $span->setTag('error', true);
            $span->log(['message', $e->getMessage(), 'code' => $e->getCode(), 'stacktrace' => $e->getTraceAsString()]);
            throw $e;
        } finally {
            $span->finish();
        }
        return $result;
    }
}