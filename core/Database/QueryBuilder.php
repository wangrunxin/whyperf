<?php

namespace Whyperf\Database;

use Hyperf\Database\Query\Builder;

class QueryBuilder extends Builder
{
    /**
     * @var bool
     */
    protected $traceable = true;

    /**
     * @return $this
     */
    function disableTrace(): self
    {
        $this->traceable = false;
        return $this;
    }

    function isTraceable(): bool
    {
        return $this->traceable;
    }
}