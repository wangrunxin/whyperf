<?php

namespace Whyperf\Http\Aspect;


use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\Dispatcher\AbstractRequestHandler;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Whyperf\Http\Request;
use Whyperf\System\CoroutineEnv\CoreGo;

/**
 * @Aspect(
 *   classes={
 *     "Hyperf\HttpServer\ResponseEmitter::emit",
 *   }
 * )
 */
class ResponseEmitterAspect extends AbstractAspect
{

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        [$response, $swooleResponse, $withContent] = $proceedingJoinPoint->getArguments();

        if (is_null($swooleResponse)) {
            return CoreGo::getCoreGo()->setResponse($response);
        }

        return $proceedingJoinPoint->process();
    }
}