<?php

namespace Whyperf\Http\Aspect;


use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\Dispatcher\AbstractRequestHandler;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Whyperf\Http\Request;
use Whyperf\System\CoroutineEnv\CoreGo;

/**
 * @Aspect(
 *   classes={
 *     "Hyperf\HttpMessage\Server\Request::loadFromSwooleRequest",
 *   }
 * )
 */
class RequestAspect extends AbstractAspect
{

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $params = $proceedingJoinPoint->getArguments();
        $swooleRequest = $params[0];

        if ($swooleRequest instanceof ServerRequestInterface) {
            return $this->initFromRequest($swooleRequest);
        }

        return $proceedingJoinPoint->process();

    }

    protected function initFromRequest(ServerRequestInterface $request)
    {
        return new \Hyperf\HttpMessage\Server\Request($request->getMethod(), $request->getUri(), $request->getHeaders(), $request->getBody(), $request->getProtocolVersion());
    }
}