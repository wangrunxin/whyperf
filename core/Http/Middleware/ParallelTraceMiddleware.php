<?php

namespace Whyperf\Http\Middleware;

use Hyperf\HttpMessage\Exception\HttpException;
use Hyperf\HttpServer\MiddlewareManager;
use Hyperf\HttpServer\Router\Dispatched;
use Hyperf\HttpServer\Server;
use Hyperf\Tracer\Middleware\TraceMiddleware;
use Hyperf\Tracer\SpanStarter;
use Hyperf\Tracer\SpanTagManager;
use Hyperf\Tracer\SwitchManager;
use Hyperf\Utils\Context;
use Hyperf\Utils\Coordinator\Constants;
use Hyperf\Utils\Coordinator\CoordinatorManager;
use Hyperf\Utils\Coroutine;
use Hyperf\Utils\Exception\ParallelExecutionException;
use Hyperf\Utils\Parallel;
use OpenTracing\Span;
use OpenTracing\Tracer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Whyperf\System\CoroutineEnv\CoreGo;
use Whyperf\Whyperf;

class ParallelTraceMiddleware extends TraceMiddleware
{

    use SpanStarter;

    /**
     * @var SwitchManager
     */
    protected $switchManager;

    /**
     * @var SpanTagManager
     */
    protected $spanTagManager;

    /**
     * @var Tracer
     */
    private $tracer;

    public function __construct(Tracer $tracer, SwitchManager $switchManager, SpanTagManager $spanTagManager)
    {
        $this->tracer = $tracer;
        $this->switchManager = $switchManager;
        $this->spanTagManager = $spanTagManager;
    }

    /**
     * Process an incoming server request.
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (is_null(CoreGo::getCoreGo())) {
            CoreGo::getInstance()
                ->setMainCore()
                ->setStartAt();
        };

        $coreGo = CoreGo::getCoreGo();

        defer(function () {
            try {
                $this->tracer->flush();
            } catch (\Throwable $exception) {
            }
        });

        try {
            $span = null;
            $span = $this->buildSpan($request);
            $coreGo->initTracerRoot();
            $response = Context::get(ResponseInterface::class);

            $parallel = new Parallel();
            $parallel->add(function () use ($request, $handler, &$response, &$coreGo, &$span) {
                CoreGo::getInstance()->setCore($coreGo->setSpan($span));
                $response = $handler->handle($request);
            });
            $parallel->wait(true);
            $span->setTag($this->spanTagManager->get('response', 'status_code'), "" . $response->getStatusCode());
        } catch (\Throwable $exception) {
            if ($exception instanceof ParallelExecutionException && count($exception->getThrowables()) == 1) {
                $exception = $exception->getThrowables()[0];
            }

            $this->appendException($span, $exception);
            throw $exception;
        } finally {
            CoreGo::Run(function () use ($span) {
                $span->finish();
            });
        }

        return $response;
    }

    protected function appendException($span, $exception)
    {
        if (!is_null($span)) {
            $this->switchManager->isEnable('error') && $this->appendExceptionToSpan($span, $exception);
            if ($exception instanceof HttpException) {
                $span->setTag($this->spanTagManager->get('response', 'status_code'), "" . $exception->getStatusCode());
            } else {
                $span->setTag($this->spanTagManager->get('response', 'status_code'), "500");
            }
        }
    }

    protected function appendExceptionToSpan(Span $span, \Throwable $exception): void
    {
        $span->setTag("error", "true");
        $span = $this->startSpan('error: ' . $exception->getMessage());
        $span->setTag($this->spanTagManager->get('exception', 'class'), get_class($exception));
        $span->setTag($this->spanTagManager->get('exception', 'code'), sprintf("%s", $exception->getCode()));
        $span->setTag($this->spanTagManager->get('exception', 'stack_trace'), (string)$exception);
        $span->finish();
    }

    protected function buildSpan(ServerRequestInterface $request): Span
    {
        $uri = $request->getUri();
        $span = $this->startSpan('request: ' . $request->getRequestTarget());
        $span->setTag($this->spanTagManager->get('coroutine', 'id'), (string)Coroutine::id());
        $span->setTag($this->spanTagManager->get('request', 'path'), (string)$uri);
        $span->setTag($this->spanTagManager->get('request', 'method'), $request->getMethod());
        foreach ($request->getHeaders() as $key => $value) {
            $span->setTag($this->spanTagManager->get('request', 'header') . '.' . $key, implode(', ', $value));
        }
        return $span;
    }
}