<?php

namespace Whyperf\Http;

use Hyperf\ExceptionHandler\ExceptionHandlerDispatcher;
use Hyperf\HttpServer\MiddlewareManager;
use Hyperf\HttpServer\ResponseEmitter;
use Hyperf\HttpServer\Router\Dispatched;
use Hyperf\JsonRpc\HttpServer;
use Hyperf\Rpc\ProtocolManager;
use Hyperf\RpcServer\RequestDispatcher;
use Hyperf\Utils\Coordinator\Constants;
use Hyperf\Utils\Coordinator\CoordinatorManager;
use Psr\Container\ContainerInterface;

class JsonRpcServerWithoutWorker extends HttpServer
{

    function __construct(ContainerInterface $container, RequestDispatcher $dispatcher, ExceptionHandlerDispatcher $exceptionHandlerDispatcher, ResponseEmitter $responseEmitter, ProtocolManager $protocolManager)
    {
        return parent::__construct($container, $dispatcher, $exceptionHandlerDispatcher, new ResponseEmitter(), $protocolManager);
    }

    /**
     * @var bool
     */
    protected bool $worker = true;

    /**
     * @return $this
     * @author WANG RUNXIN
     */
    function disableWaiting(): self
    {
        $this->worker = false;
        return $this;
    }

    public function onRequest($request, $response): void
    {
        try {
            if ($this->worker) {
                CoordinatorManager::until(Constants::WORKER_START)->yield();
            }

            [$psr7Request, $psr7Response] = $this->initRequestAndResponse($request, $response);
            $psr7Request = $this->coreMiddleware->dispatch($psr7Request);
            /** @var Dispatched $dispatched */
            $dispatched = $psr7Request->getAttribute(Dispatched::class);
            $middlewares = $this->middlewares;
            if ($dispatched->isFound()) {
                $registeredMiddlewares = MiddlewareManager::get($this->serverName, $dispatched->handler->route, $psr7Request->getMethod());
                $middlewares = array_merge($middlewares, $registeredMiddlewares);
            }
            $psr7Response = $this->dispatcher->dispatch($psr7Request, $middlewares, $this->coreMiddleware);
        } catch (Throwable $throwable) {
            // Delegate the exception to exception handler.
            $psr7Response = $this->exceptionHandlerDispatcher->dispatch($throwable, $this->exceptionHandlers);
        } finally {
            $this->worker = true;

            // Send the Response to client.
            if (!isset($psr7Response)) {
                return;
            }
            if (isset($psr7Request) && $psr7Request->getMethod() === 'HEAD') {
                $this->responseEmitter->emit($psr7Response, $response, false);
            } else {
                $this->responseEmitter->emit($psr7Response, $response, true);
            }
        }
    }
}