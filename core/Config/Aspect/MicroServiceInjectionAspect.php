<?php

namespace Whyperf\Config\Aspect;


use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\Dispatcher\AbstractRequestHandler;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Whyperf\Rpc\ConsumerClient;
use Whyperf\Rpc\ServiceServer;
use Whyperf\System\CoroutineEnv\CoreGo;

/**
 * @Aspect(
 *   classes={
 *     "Hyperf\Config\ConfigFactory::readPaths",
 *   }
 * )
 */
class MicroServiceInjectionAspect extends AbstractAspect
{
    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $configs = $proceedingJoinPoint->process();
        $configs = ServiceServer::addToConfig($configs);
        return ConsumerClient::addToConfig($configs);
    }
}