<?php

namespace Whyperf\Config;

use Hyperf\Config\Config;
use Hyperf\Config\ProviderConfig;
use Psr\Container\ContainerInterface;
use Symfony\Component\Finder\Finder;
use Whyperf\Rpc\ConsumerClient;
use Whyperf\Rpc\ServiceServer;

/**
 * Class ConfigFactory
 * @package whyperf\Config
 * @author WANG RUNXIN
 */
class ConfigFactory extends \Hyperf\Config\ConfigFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $configPath = BASE_PATH . '/config/';
        $config = $this->readConfig($configPath . 'config.php');
        $autoloadConfig = $this->readPaths([BASE_PATH . '/config/autoload']);
        $merged = array_merge_recursive(ProviderConfig::load(), $config, ...$autoloadConfig);
        return new Config($merged);
    }

    private function readConfig(string $configPath): array
    {
        $config = [];
        if (file_exists($configPath) && is_readable($configPath)) {
            $config = require $configPath;
        }
        return is_array($config) ? $config : [];
    }

    private function readPaths(array $paths)
    {
        $configs = [];
        $finder = new Finder();
        $finder->files()->in($paths)->name('*.php');
        foreach ($finder as $file) {
            $configs[] = [
                $file->getBasename('.php') => require $file->getRealPath(),
            ];
        }

        $configs = ServiceServer::addToConfig($configs);
        return ConsumerClient::addToConfig($configs);
    }
}