<?php

namespace Whyperf\MultiTenant\Middleware;

use Hyperf\Contract\ContainerInterface;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Whyperf\Authenticator\Authenticator;
use Whyperf\Model\Traits\CoroutineSafeSingleton;
use Whyperf\MultiTenant\MultiTenantAuthenticator;
use Whyperf\System\CoroutineEnv\CoComponent;
use Hyperf\HttpServer\Contract\ResponseInterface as HttpResponse;
use Whyperf\System\CoroutineEnv\CoreGo;

class MultiTenantMW implements MiddlewareInterface
{

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return MultiTenantAuthenticator::auth($request, function ($request) use ($handler){
            return $handler->handle($request);
        });
    }
}