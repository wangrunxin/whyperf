<?php

namespace Whyperf\MultiTenant\DbConnection;

use Hyperf\Database\ConnectionInterface;
use Hyperf\Database\ConnectionResolverInterface;
use Hyperf\DbConnection\Db;
use Hyperf\Utils\ApplicationContext;
use Psr\Container\ContainerInterface;
use Whyperf\Database\QueryBuilder;

/**
 * Class Connection
 * /**
 * DB Helper.
 * @method static QueryBuilder table(string $table)
 * used for create multiple-tenant query
 * @package Whyperf\MultiTenant\DbConnection
 * @author WANG RUNXIN
 */
class Connection extends Db
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __call($name, $arguments)
    {
        if ($name === 'connection') {
            return $this->__connection(...$arguments);
        }
        return $this->__connection()->{$name}(...$arguments);
    }

    public static function __callStatic($name, $arguments)
    {
        $db = ApplicationContext::getContainer()->get(Connection::class);
        if ($name === 'connection') {
            return $db->__connection(...$arguments);
        }
        return $db->__connection()->{$name}(...$arguments);
    }

    private function __connection($pool = 'default'): ConnectionInterface
    {
        $resolver = $this->container->get(MultiTenantConnectionResolver::class);
        return $resolver->connection($pool);
    }
}