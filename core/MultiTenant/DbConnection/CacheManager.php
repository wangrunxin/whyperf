<?php

namespace Whyperf\MultiTenant\DbConnection;

use Hyperf\Database\Model\Model;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\ModelCache\Config;
use Hyperf\ModelCache\Handler\HandlerInterface;
use Hyperf\ModelCache\Handler\RedisHandler;
use Hyperf\ModelCache\Manager;
use Whyperf\MultiTenant\MultiTenantAuthenticator;
use Whyperf\Whyperf;

class CacheManager extends Manager
{

    /**
     * @param $key
     * @param $config
     */
    function addHandler($key, $config)
    {
        if ($this->hasHandler($key)) {
            return;
        }

        $this->setHandler($key, $config);
    }

    function addHanldlerByDbConfig($key, array $config)
    {
        if (!isset($config["cache"]) || empty($config["cache"])) {
            return;
        }

        $this->addHandler($key, $config);
    }

    /**
     * Fetch a model from cache only.
     * @param mixed $id
     */
    public function findFromCacheOnly($id, string $class): ?Model
    {
        /** @var Model $instance */
        $instance = new $class();

        $name = $instance->getConnectionName();
        $primaryKey = $instance->getKeyName();

        if ($handler = $this->handlers[$name] ?? null) {
            $key = $this->getCacheKey($id, $instance, $handler->getConfig());
            $data = $handler->get($key);
            if ($data) {
                return $instance->newFromBuilder(
                    $this->getAttributes($handler->getConfig(), $instance, $data)
                );
            }

            // It not exist in cache handler.
            return null;
        }

        return false;
    }

    /**
     * @param $key
     * @param $config
     */
    function setHandler($key, $item)
    {
        $handlerClass = $item['cache']['handler'] ?? RedisHandler::class;
        $config = new Config($item['cache'] ?? [], $key);
        $this->handlers[$key] = make($handlerClass, ['config' => $config]);
    }

    /**
     * @param $key
     * @return bool
     */
    function hasHandler($key): bool
    {
        return isset($this->handlers[$key]);
    }

    /**
     * @param int|string $id
     */
    protected function getCacheKey($id, Model $model, Config $config): string
    {
        // mc:$prefix:m:$model:$pk:$id
        return sprintf(
            $config->getCacheKey(),
            $config->getPrefix(),
            sprintf("tenant-%s-%s", MultiTenantAuthenticator::getInstance()->getTenantId(), $model->getTable()),
            $model->getKeyName(),
            $id
        );
    }

    function updateCache(\Whyperf\Model\Model $instance): bool
    {
        $name = $instance->getConnectionName();
        $primaryKey = $instance->getKeyName();

        if ($handler = $this->handlers[$name] ?? null) {
            if (is_null($instance->$primaryKey)) {
                return false;
            }
            $key = $this->getCacheKey($instance->$primaryKey, $instance, $handler->getConfig());
            $ttl = $this->getCacheTTL($instance, $handler);
            return $handler->set($key, $this->formatModel($instance), $ttl);
        }

        return false;
    }

    function reloadCache(\Whyperf\Model\Model $instance)
    {
        $name = $instance->getConnectionName();
        $primaryKey = $instance->getKeyName();

        if ($handler = $this->handlers[$name] ?? null) {
            if (is_null($instance->$primaryKey)) {
                return null;
            }
            $key = $this->getCacheKey($instance->$primaryKey, $instance, $handler->getConfig());
            $data = $handler->get($key);
            if ($data) {
                return $instance->newFromBuilder(
                    $this->getAttributes($handler->getConfig(), $instance, $data)
                );
            }
        }

        return null;
    }

    /**
     * @return CacheManager
     */
    static function getInstance(): CacheManager
    {
        return Whyperf::getContainer()->get(Manager::class);
    }
}