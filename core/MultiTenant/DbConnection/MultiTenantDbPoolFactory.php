<?php

namespace Whyperf\MultiTenant\DbConnection;

use Hyperf\DbConnection\Pool\DbPool;
use Hyperf\Di\Container;

class MultiTenantDbPoolFactory extends \Hyperf\DbConnection\Pool\PoolFactory{

    public function getPoolWithOriginalName(string $name, string  $originalName): DbPool
    {
        if (isset($this->pools[$name])) {
            return $this->pools[$name];
        }

        if ($this->container instanceof Container) {
            $pool = $this->container->make(MultiTenantDbPool::class, ['name' => $name, 'originalName' => $originalName]);
        } else {
            $pool = new MultiTenantDbPool($this->container, $name, $originalName);
        }

        return $this->pools[$name] = $pool;
    }
}