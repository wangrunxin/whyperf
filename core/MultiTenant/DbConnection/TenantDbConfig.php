<?php

namespace Whyperf\MultiTenant\DbConnection;

use Whyperf\Model\Traits\Singleton;
use Whyperf\MultiTenant\MultiTenantAuthenticator;
use Whyperf\Whyperf;

class TenantDbConfig
{
    use Singleton;

    protected $tenantDbConfigs = [];

    function get($name)
    {
        if (!isset($this->tenantDbConfigs[$name])) {
            $configFactory = Whyperf::getContainer()->getWithDefault($name);
            if (is_null($configFactory)) {
                $configFactory = $this->create($name);
            }

            $this->tenantDbConfigs[$name] = $configFactory->getConfig();
        }
        return $this->tenantDbConfigs[$name];
    }

    function create($name): DbConnectionConfig
    {
        $configFactory = Whyperf::getContainer()->make(DbConnectionConfig::class, ["name" => $name]);

        Whyperf::getContainer()->set($name, $configFactory);

        return $configFactory;
    }
}