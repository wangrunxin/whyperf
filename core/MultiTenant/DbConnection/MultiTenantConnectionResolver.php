<?php

namespace Whyperf\MultiTenant\DbConnection;

use Hyperf\Database\ConnectionInterface;
use Hyperf\Database\ConnectionResolverInterface;
use Hyperf\DbConnection\ConnectionResolver;
use Hyperf\DbConnection\Pool\PoolFactory;
use Hyperf\Utils\Context;
use Hyperf\Utils\Coroutine;
use Psr\Container\ContainerInterface;
use Whyperf\Database\ConnectionManager;
use Whyperf\MultiTenant\MultiTenantAuthenticator;

/**
 * Class MultiTenantConnectionResolver
 * @package whyperf\MultiTenant\DbConnection
 * @author WANG RUNXIN
 */
class MultiTenantConnectionResolver extends ConnectionResolver
{
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->factory = $container->get(MultiTenantDbPoolFactory::class);
    }

    /**
     * Get a database connection instance.
     *
     * @param string $name
     * @return ConnectionInterface
     */
    public function connection($name = null)
    {
        if (is_null($name)) {
            $name = $this->getDefaultConnection();
        }

        $originalName = $name;
        $name = $this->addTenantId($name);

        $connection = null;
        $id = $this->getContextKey($name);

        if (Context::has($id)) {
            $connection = Context::get($id);
        }

        if (!$connection instanceof ConnectionInterface) {
            $pool = $this->factory->getPoolWithOriginalName($name, $originalName);
            $connection = $pool->get();
            try {
                // PDO is initialized as an anonymous function, so there is no IO exception,
                // but if other exceptions are thrown, the connection will not return to the connection pool properly.
                $connection = $connection->getConnection();
                Context::set($id, $connection);
                ConnectionManager::add($id, $name);
            } finally {
                if (Coroutine::inCoroutine()) {
                    defer(function (){
                        ConnectionManager::release();
                    });
                }
            }
        }

        return $connection;
    }

    /**
     * The key to identify the connection object in coroutine context.
     * @param mixed $name
     */
    private function getContextKey($name): string
    {
        return sprintf('database.connection.%s', $name);
    }

    private function addTenantId($name)
    {
        return sprintf('MT-%s.%s', MultiTenantAuthenticator::getInstance()->getTenantId(), $name);
    }
}