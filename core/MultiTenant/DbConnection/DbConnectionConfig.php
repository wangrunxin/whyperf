<?php

namespace Whyperf\MultiTenant\DbConnection;

abstract class DbConnectionConfig
{
    protected $name;

    abstract function getDatebaseName(): string;

    abstract function getUsername(): string;

    abstract function getPassword(): string;

    abstract function getHost(): string;

    function __construct($name)
    {
        $this->name = $name;
    }

    function getPort(): int
    {
        return 3306;
    }

    function getCharset(): string
    {
        return env('DB_CHARSET', "utf8mb4");
    }

    function getCollation(): string
    {
        return env('DB_COLLATION', "utf8mb4_unicode_ci");
    }

    function getDriver(): string
    {
        return env('DB_DRIVER', 'mysql');
    }

    function getPrefix(): string
    {
        return env('DB_PREFIX', '');
    }

    function getPoolConfig(): array
    {
        return config("tenant_database.pool", [
            'min_connections' => 1,
            'max_connections' => 10,
            'connect_timeout' => 10.0,
            'wait_timeout' => 3.0,
            'heartbeat' => -1,
            'max_idle_time' => (float)env('DB_MAX_IDLE_TIME', 60),
        ]);
    }

    function getCacheConfig(): array
    {
        return [
//             'handler' => Hyperf\ModelCache\Handler\RedisHandler::class,
//             'cache_key' => '{mc:%s:m:%s}:%s:%s',
//             'prefix' => 'default',
//             'ttl' => 3600 * 24,
//             'empty_model_ttl' => 600,
//             'load_script' => true,
        ];
    }

    function getCommands()
    {
        return [
            'gen:model' => [
                'path' => 'app/Model',
                'force_casts' => true,
                'inheritance' => 'Model',
                'uses' => '',
                'table_mapping' => [],
            ],
        ];
    }

    function getConfig()
    {
        return [
            'driver' => $this->getDriver(),
            'host' => $this->getHost(),
            'port' => $this->getPort(),
            'database' => $this->getDatebaseName(),
            'username' => $this->getUsername(),
            'password' => $this->getPassword(),
            'charset' => $this->getCharset(),
            'collation' => $this->getCollation(),
            'prefix' => $this->getPrefix(),
            'pool' => $this->getPoolConfig(),
            'cache' => $this->getCacheConfig(),
            'commands' => $this->getCommands(),
        ];
    }
}