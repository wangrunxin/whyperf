<?php

namespace Whyperf\MultiTenant\DbConnection;

use Hyperf\Contract\ConfigInterface;
use Hyperf\Contract\ConnectionInterface;
use Hyperf\DbConnection\Connection;
use Hyperf\DbConnection\Db;
use Hyperf\DbConnection\Frequency;
use Hyperf\DbConnection\Pool\DbPool;
use Hyperf\Pool\Pool;
use Hyperf\Utils\Arr;
use Psr\Container\ContainerInterface;
use Whyperf\MultiTenant\MultiTenantAuthenticator;

class MultiTenantDbPool extends DbPool
{
    protected $name;

    protected $config;

    public function __construct(ContainerInterface $container, string $name, string $originalName)
    {
        $this->name = $name;
        $config = $container->get(ConfigInterface::class);
        $key = sprintf('databases.%s', $this->name);

        if (!$config->has($key)) {
            $multiTenantConfig = TenantDbConfig::getInstance()->get($key);
            if (is_null($multiTenantConfig)) {
                throw new \InvalidArgumentException(sprintf('config[%s] is not exist!', $key));
            }
            $config->set("{$key}.name", $name);
            $config->set($key, $multiTenantConfig);
            $this->config = $multiTenantConfig;
            CacheManager::getInstance()->addHanldlerByDbConfig($originalName, $multiTenantConfig);
        } else {
            // Rewrite the `name` of the configuration item to ensure that the model query builder gets the right connection.
            $config->set("{$key}.name", $name);
            $this->config = $config->get($key);
        }

        $options = Arr::get($this->config, 'pool', []);

        $this->frequency = make(Frequency::class, [$this]);
        Pool::__construct($container, $options);
    }

    private function addTenantId($name)
    {
        return sprintf('MT-%s.%s', MultiTenantAuthenticator::getInstance()->getTenantId(), $name);
    }

    protected function createConnection(): ConnectionInterface
    {
        return new Connection($this->container, $this, $this->config);
    }
}