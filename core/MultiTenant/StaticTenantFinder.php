<?php

namespace Whyperf\MultiTenant;

use Whyperf\System\CoroutineEnv\AbstractCoreGoAssert;
use Whyperf\System\CoroutineEnv\MiddlewareAssert;

/**
 * Class StaticTenantFinder
 * use 215 as tenant id forever
 * @package Whyperf\MultiTenant
 * @author WANG RUNXIN
 */
class StaticTenantFinder extends AbstractTenantFinder
{
    function run(MiddlewareAssert $middlewareAssert)
    {
        /**
         * @var MultiTenantAuthenticator $middlewareAssert
         */
        $middlewareAssert->setTenantId(215);
    }
}