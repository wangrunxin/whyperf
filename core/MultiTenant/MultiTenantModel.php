<?php

namespace Whyperf\MultiTenant;

use Hyperf\Database\ConnectionInterface;
use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Collection;
use Hyperf\DbConnection\Connection;
use Hyperf\DbConnection\Model\Model;
use Hyperf\ModelCache\Cacheable;
use Hyperf\ModelCache\CacheableInterface;
use Whyperf\Database\QueryBuilder;
use Whyperf\MultiTenant\DbConnection\MultiTenantConnectionResolver;
use Whyperf\Whyperf;

class MultiTenantModel extends \Whyperf\Model\Model
{

    const CREATED_AT = null;

    const UPDATED_AT = null;

    protected $enableAudit = true;

    protected $connectionModel;

    /**
     * Get the database connection for the model.
     * You can write it by yourself.
     */
    public function getConnection(): ConnectionInterface
    {
        return Whyperf::getContainer()->get(MultiTenantConnectionResolver::class)->connection($this->getConnectionName());
    }

    /**
     * Get a new query builder instance for the connection.
     *
     * @return \Hyperf\Database\Query\Builder
     */
    protected function newBaseQueryBuilder()
    {
        $connection = $this->getConnection();

        $builder = new QueryBuilder($connection, $connection->getQueryGrammar(), $connection->getPostProcessor());

        if (!$this->enableAudit) {
            $builder->disableTrace();
        }

        return $builder;
    }

    /**
     * Set the connection associated with the model.
     *
     * @param string $name
     * @return $this
     */
    public function setConnection($name)
    {
        if (is_null($name)) {
            //hyperf bug
            return $this;
        }

        $this->connection = $name;

        return $this;
    }
}