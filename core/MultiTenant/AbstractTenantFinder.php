<?php


namespace Whyperf\MultiTenant;

use Hyperf\HttpMessage\Base\Response;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\Utils\Context;
use Psr\Http\Message\ResponseInterface;
use Whyperf\System\CoroutineEnv\AbstractCoreGoAssert;
use Whyperf\System\CoroutineEnv\MiddlewareAssert;

abstract class AbstractTenantFinder extends AbstractCoreGoAssert{
}