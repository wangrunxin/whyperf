<?php

namespace Whyperf\MultiTenant;

use Hyperf\Database\ConnectionInterface;
use Hyperf\Database\Schema\Schema;
use Hyperf\Utils\ApplicationContext;
use Whyperf\MultiTenant\DbConnection\MultiTenantConnectionResolver;

/**
 * Class MultiTeanntSchema
 * @package Whyperf\MultiTenant
 * @author WANG RUNXIN
 */
class MultiTeanntSchema extends Schema
{
    /**
     * Create a connection by ConnectionResolver.
     */
    public function connection(string $name = 'default'): ConnectionInterface
    {
        $container = ApplicationContext::getContainer();
        $resolver = $container->get(MultiTenantConnectionResolver::class);
        return $resolver->connection($name);
    }
}
