<?php

namespace Whyperf\MultiTenant;

use Hyperf\Contract\ContainerInterface;
use Hyperf\HttpServer\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Whyperf\Model\Traits\CoroutineSafeSingleton;
use Whyperf\System\CoroutineEnv\CoComponent;
use Hyperf\HttpServer\Contract\ResponseInterface as HttpResponse;
use Whyperf\System\CoroutineEnv\CoreGo;
use Whyperf\System\CoroutineEnv\MiddlewareAssert;
use Whyperf\Whyperf;

/**
 * Class MultiTenantAuthenticator
 * @package Whyperf\MultiTenant\Middleware
 * @author WANG RUNXIN
 */
class MultiTenantAuthenticator extends MiddlewareAssert
{
    /**
     * @var int
     */
    protected $tenantId;

    function getAssertClass(): string
    {
        return AbstractTenantFinder::class;
    }

    function getTenantId()
    {
        return $this->tenantId;
    }

    function setTenantId($id)
    {
        $this->tenantId = $id;
    }

    function attachedJsonRpc(&$json)
    {
        $json["header"]["tenant_id"] = $this->getTenantId();
    }
}