<?php

namespace Whyperf\Log;

use Hyperf\Tracer\SpanStarter;
use Hyperf\Tracer\SpanTagManager;
use Hyperf\Tracer\SwitchManager;
use Monolog\DateTimeImmutable;
use Monolog\Handler\HandlerInterface;
use OpenTracing\Tracer;
use Whyperf\Whyperf;

class ZipkinLogger extends \Hyperf\Logger\Logger
{

    use SpanStarter;

    /**
     * @var Tracer
     */
    private $tracer;

    /**
     * @var SwitchManager
     */
    private $switchManager;

    /**
     * @var SpanTagManager
     */
    private $spanTagManager;

    /**
     * @psalm-param array<callable(array): array> $processors
     *
     * @param string $name The logging channel, a simple descriptive name that is attached to all log records
     * @param HandlerInterface[] $handlers Optional stack of handlers, the first one in the array is called first, etc.
     * @param callable[] $processors Optional array of processors
     * @param DateTimeZone|null $timezone Optional timezone, if not provided date_default_timezone_get() will be used
     */
    public function __construct(string $name, array $handlers = [], array $processors = [], ?DateTimeZone $timezone = null)
    {
        parent::__construct($name, $handlers, $processors, $timezone);
        $this->init();
    }

    protected function init()
    {
        $this->tracer = Whyperf::getContainer()->get(Tracer::class);
        $this->switchManager = Whyperf::getContainer()->get(SwitchManager::class);
        $this->spanTagManager = Whyperf::getContainer()->get(SpanTagManager::class);
    }

    /**
     * Adds a log record.
     *
     * @param int $level The logging level
     * @param string $message The log message
     * @param mixed[] $context The log context
     * @return bool    Whether the record has been processed
     *
     * @phpstan-param Level $level
     */
    public function addRecord(int $level, string $message, array $context = [], DateTimeImmutable $datetime = null): bool
    {
        $span = $this->startSpan("log: " . $message);
        $span->setTag($this->spanTagManager->get('log', 'level'), self::$levels[$level]);
        $span->setTag($this->spanTagManager->get('log', 'context'), print_r($context, true));
        $span->finish();

        return true;
    }
}
