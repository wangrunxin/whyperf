<?php

namespace Whyperf\Log;

class Logger extends \Hyperf\Logger\Logger {

    /**
     * Error message level.
     * An error message is one that indicates the abnormal termination of the
     * application and may require developer's handling.
     */
    const LEVEL_ERROR = 0x01;

    /**
     * Warning message level.
     * A warning message is one that indicates some abnormal happens but
     * the application is able to continue to run. Developers should pay attention to this message.
     */
    const LEVEL_WARNING = 0x02;

    /**
     * Informational message level.
     * An informational message is one that includes certain information
     * for developers to review.
     */
    const LEVEL_INFO = 0x04;

    /**
     * Tracing message level.
     * An tracing message is one that reveals the code execution flow.
     */
    const LEVEL_TRACE = 0x08;

    /**
     * Profiling message level.
     * This indicates the message is for profiling purpose.
     */
    const LEVEL_PROFILE = 0x40;
}