<?php
declare(strict_types=1);

namespace Whyperf;

use App\App;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;
use Whyperf\Constants\SystemConstants;
use Whyperf\Exception\Handler\BaseExceptionHandler;
use Whyperf\Model\Traits\Singleton;
use Hyperf\Contract\ApplicationInterface;
use Hyperf\Di\Definition\DefinitionSourceFactory;
use Hyperf\Utils\ApplicationContext;
use Whyperf\Di\Container;
use Whyperf\System\ClassLoader;
use Whyperf\System\Traits\UtilTrait;

/**
 * Class Whyperf
 * @package whyperf
 * @author WANG RUNXIN
 */
class Whyperf
{
    use Singleton;
    use UtilTrait;

    /**
     *
     * @var Container
     */
    private $_container;

    /**
     *
     * @var Application
     */
    private $_app;

    function __construct()
    {
        try {
            ClassLoader::init();
            $container = new Container((new DefinitionSourceFactory(true))());

            if (!$container instanceof \Psr\Container\ContainerInterface) {
                throw new \RuntimeException('The dependency injection container is invalid.');
            }
            $this->_container = ApplicationContext::setContainer($container);
        } catch (\Throwable $throwable) {
            (new BaseExceptionHandler())->handle($throwable);
            exit;
        }
    }

    static function getContainer(): Container
    {
        return Whyperf::getInstance()->getWhyperfContainer();
    }

    function getWhyperfContainer(): Container
    {
        return $this->_container;
    }

    /**
     *
     * @return Application
     */
    function getApp(): Application
    {
        if (is_null($this->_app)) {
            $this->_app = $this->getContainer()->get(ApplicationInterface::class);
        }

        return $this->_app;
    }

    /**
     *
     * @return BaseExceptionHandler
     */
    function getSystemErrorHandler(): BaseExceptionHandler
    {
        return $this->getContainer()->getWithDefault(BaseExceptionHandler::class, BaseExceptionHandler::class);
    }

    function run()
    {
        try {
            App::getInstance();
            $this->getApp()->setCatchExceptions(false);
            $this->getApp()->run();
        } catch (\Throwable $r) {
            $this->getSystemErrorHandler()->handle($r);
        }
    }

    static function logo(){
        $io = new SymfonyStyle(new ArrayInput([]), new ConsoleOutput());
        $io->text(Whyperf::whyperf());
    }

    public static function whyperf()
    {
        return <<<LOGO
           _                                 __ 
          | |                               / _|
__      __| |__   _   _  _ __    ___  _ __ | |_ 
\ \ /\ / /| '_ \ | | | || '_ \  / _ \| '__||  _|
 \ V  V / | | | || |_| || |_) ||  __/| |   | |  
  \_/\_/  |_| |_| \__, || .__/  \___||_|   |_|  
                   __/ || |                     
                  |___/ |_|     
                  
                 
LOGO;
    }
}

?>