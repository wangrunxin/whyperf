<?php

namespace Whyperf\Validator;

use Hyperf\Utils\Context;
use Hyperf\Validation\Request\FormRequest;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Whyperf\System\CoroutineEnv\CoreGo;
use Hyperf\Di\Annotation\Inject;

/**
 * Class ValidatedRequest
 * @package whyperf\Validator
 * @author WANG RUNXIN
 */
class ValidatedRequest extends FormRequest
{
    function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
    }

    /**
     * Get the proper failed validation response for the request.
     */
    public function response(): ResponseInterface
    {
        /** @var ResponseInterface $response */
        $response = Context::get(ResponseInterface::class);

        $response = $response->withStatus(422);

        CoreGo::getCoreGo()->setResponse($response);

        return $response;
    }
}