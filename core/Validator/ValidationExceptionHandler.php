<?php

namespace Whyperf\Validator;

use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\Validation\ValidationException;
use Monolog\Logger;
use Psr\Http\Message\ResponseInterface;
use Whyperf\Whyperf;

class ValidationExceptionHandler extends \Hyperf\Validation\ValidationExceptionHandler
{

    public function handle(\Throwable $throwable, ResponseInterface $response)
    {
        $this->stopPropagation();
        /**
         * @var ValidationException $throwable
         */

        if (!$response->hasHeader('content-type')) {
            $response = $response->withAddedHeader('content-type', 'text/json; charset=utf-8');
        }

        $errors = $throwable->validator->errors();

        Whyperf::getLogger("auth")->log(Logger::WARNING, $errors);

        return $response->withStatus($throwable->status)->withBody(new SwooleStream($errors));
    }
}