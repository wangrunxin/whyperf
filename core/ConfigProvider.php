<?php

declare(strict_types=1);

namespace Whyperf;

use Hyperf\DbConnection\ConnectionResolver;
use Hyperf\HttpServer\Exception\Handler\HttpExceptionHandler;
use Hyperf\Logger\Logger;
use Hyperf\ModelCache\Manager;
use Whyperf\Log\ZipkinLogger;
use Whyperf\MultiTenant\DbConnection\CacheManager;

class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'dependencies' => [
                HttpExceptionHandler::class => \Whyperf\Exception\Handler\HttpExceptionHandler::class,
                Manager::class => CacheManager::class,
                Configuration::class => ConfigFactory::class,
                Logger::class => ZipkinLogger::class,
                ConnectionResolver::class => ConnectionResolver::class
            ],
            'annotations' => [
                'scan' => [
                    'paths' => [
                        __DIR__ . DIRECTORY_SEPARATOR,
                    ],
                ],
                'class_map' => [
                    ConnectionResolver::class => Whyperf::getPath() . "Database/class_map/ConnectionResolver.php"
                ]
            ],
            'publish' => [
                [
                    'id' => 'jsonrpc-config',
                    'description' => 'jsonrpc config file',
                    'source' => __DIR__ . '/../publish/jsonrpc.php',
                    'destination' => BASE_PATH . '/config/autoload/jsonrpc.php',
                ],
                [
                    'id' => 'tenant-db',
                    'description' => 'tenant db config file',
                    'source' => __DIR__ . '/../publish/tenant_database.php',
                    'destination' => BASE_PATH . '/config/autoload/tenant_database.php',
                ],
            ],
        ];
    }
}
