<?php

namespace Whyperf\Authenticator;

use Hyperf\HttpMessage\Base\Response;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\Utils\Context;
use Psr\Http\Message\ResponseInterface;
use Whyperf\System\CoroutineEnv\CoreGo;
use Whyperf\System\CoroutineEnv\MiddlewareAssert;

class Superman extends AbstractDefender
{

    function run(MiddlewareAssert $middlewareAssert)
    {
        /** @var ResponseInterface $response */
        $response = Context::get(ResponseInterface::class)
            ->withStatus(self::STATUS_UNAUTHORIZED)
            ->withBody(
                new SwooleStream(
                    json_encode([
                        "code" => self::STATUS_UNAUTHORIZED,
                        "result" => Response::getReasonPhraseByCode(self::STATUS_UNAUTHORIZED),
                        "message" => "superman never surrender."
                    ])
                )
            );

        $middlewareAssert->setFalseResult($response);
    }
}