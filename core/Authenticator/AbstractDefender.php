<?php

namespace Whyperf\Authenticator;

use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ServerRequestInterface;
use Whyperf\System\CoroutineEnv\AbstractCoreGoAssert;

abstract class AbstractDefender extends AbstractCoreGoAssert implements StatusCodeInterface
{
    protected $user;

    function getUser(){
        return $this->user;
    }

    function setUser($user){
        $this->user = $user;
    }

    function addRpcAuthType(&$json)
    {
    }
}
