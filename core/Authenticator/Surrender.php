<?php

namespace Whyperf\Authenticator;

use Hyperf\Utils\Context;
use Psr\Http\Message\ResponseInterface;
use Richard\HyperfPassport\Bridge\User;
use Whyperf\System\CoroutineEnv\CoreGo;
use Whyperf\System\CoroutineEnv\MiddlewareAssert;

class Surrender extends AbstractDefender {

    function getUser()
    {
        return new User("id");
    }

    function run(MiddlewareAssert $middlewareAssert)
    {
        //always pass;
    }
}