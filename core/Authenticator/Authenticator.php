<?php

namespace Whyperf\Authenticator;

use Whyperf\System\CoroutineEnv\MiddlewareAssert;

class Authenticator extends MiddlewareAssert
{
    const AUTH_TYPE = "authType";
    const AUTH_SECRET = "authSecret";

    protected $authSecret;

    public function getAssertClass(): string
    {
        return AbstractDefender::class;
    }

    function setAuthSecret($authSecret)
    {
        $this->authSecret = $authSecret;
    }

    function getAuthSecret()
    {
        return $this->authSecret;
    }

    function attachedJsonRpc(&$json)
    {
        $this->addJsonRpcHeader(self::AUTH_SECRET, $this->getAuthSecret(), $json);

        /**
         * @var AbstractDefender $defender
         */
        $defender = $this->getAssert();
        $defender->addRpcAuthType($json);
    }
}

