<?php

namespace Whyperf\Authenticator\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Whyperf\Authenticator\Authenticator;
use Whyperf\MultiTenant\MultiTenantAuthenticator;
use Whyperf\System\CoroutineEnv\CoreGo;

class AuthMW implements MiddlewareInterface
{

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return Authenticator::auth($request, function ($request) use ($handler){
            return $handler->handle($request);
        });
    }
}