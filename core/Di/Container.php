<?php

namespace Whyperf\Di;

use App\App;
use Hyperf\Di\Container as DefaultContainer;
use Hyperf\Di\Exception\InvalidDefinitionException;
use Hyperf\Di\Exception\NotFoundException;

/**
 * Class Container
 * @package whyperf\Di
 * @author WANG RUNXIN
 */
class Container extends DefaultContainer
{

    public function getWithDefault($name, $default = null)
    {
        try {
            return $this->get($name);
        } catch (NotFoundException $e) {
            if (is_null($default)) {
                return $default;
            }
            $this->define($name, $default);
            return $this->getWithDefault($name, $default);
        }
    }

    public function defineWithDefault($name, $value)
    {
        try {
            return $this->get($name);
        } catch (NotFoundException $e) {
            if (is_null($value)) {
                return null;
            }
            return $this->define($name, $value);
        } catch (InvalidDefinitionException $e) {
            if (is_null($value)) {
                return null;
            }
            return $this->define($name, $value);
        }
    }

    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param string $name identifier of the entry to look for
     */
    public function get($name)
    {
        $entry = parent::get($name);

        if (!is_object($entry)) {
            return $entry;
        }

        if (App::isLazyMakeClass($entry)) {
            return $this->make($name);
        }

        return $entry;
    }
}

?>