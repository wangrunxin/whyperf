<?php

namespace Whyperf\Tracker\Aspect;


use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\Dispatcher\AbstractRequestHandler;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Whyperf\Audit\Audit;
use Whyperf\Database\QueryBuilder;
use Whyperf\Rpc\ConsumerClient;

use Whyperf\Rpc\ServiceServer;
use Whyperf\System\CoroutineEnv\CoreGo;

/**
 * @Aspect(
 *   classes={
 *     "Hyperf\Database\Query\Grammars\Grammar::compile*"
 *   }
 * )
 */
class TraceableGrammarAspect extends AbstractAspect
{
    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $result = $proceedingJoinPoint->process();
        [$builder] = $proceedingJoinPoint->getArguments();

        if(is_string($result) && $builder instanceof QueryBuilder){
            if(!$builder->isTraceable()){
//                Audit::getInstance()->disableAuditSql($result);
            }
        }

        return $result;
    }
}