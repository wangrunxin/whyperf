<?php

namespace Whyperf\Tracker\Engine;

use Hyperf\Tracer\SpanStarter;
use Hyperf\Tracer\SpanTagManager;
use Hyperf\Tracer\SwitchManager;
use Hyperf\Utils\Coroutine;
use OpenTracing\Tracer;
use Psr\Log\LoggerInterface;
use Whyperf\System\CoroutineEnv\CoreGo;

class ZipkinTracer implements LoggerInterface
{

    /**
     * @var SwitchManager
     */
    protected $switchManager;

    /**
     * @var SpanTagManager
     */
    protected $spanTagManager;

    /**
     * @var Tracer
     */
    private $tracer;

    use SpanStarter;

    public function __construct(Tracer $tracer, SwitchManager $switchManager, SpanTagManager $spanTagManager)
    {
        $this->tracer = $tracer;
        $this->switchManager = $switchManager;
        $this->spanTagManager = $spanTagManager;
    }

    public function emergency($message, array $context = array())
    {
        // TODO: Implement emergency() method.
    }

    public function alert($message, array $context = array())
    {
        // TODO: Implement alert() method.
    }

    public function critical($message, array $context = array())
    {
        // TODO: Implement critical() method.
    }

    public function error($message, array $context = array())
    {
        // TODO: Implement error() method.
    }

    public function warning($message, array $context = array())
    {
        // TODO: Implement warning() method.
    }

    public function notice($message, array $context = array())
    {
        // TODO: Implement notice() method.
    }

    public function info($message, array $context = array())
    {
        // TODO: Implement info() method.
    }

    public function debug($message, array $context = array())
    {
        // TODO: Implement debug() method.
    }

    public function log($level, $message, array $context = array())
    {
        $this->create($level, $message, $context);
    }

    protected function create($level, $message, array $context)
    {
        CoreGo::getCoreGo()->getSpan()->log([
            $level, $message, $context
        ]);
    }
}
