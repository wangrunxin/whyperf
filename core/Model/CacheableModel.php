<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace Whyperf\Model;

use Hyperf\DbConnection\Model\Model as BaseModel;
use Hyperf\ModelCache\Cacheable;
use Hyperf\ModelCache\CacheableInterface;
use Hyperf\ModelCache\Manager;
use Whyperf\MultiTenant\DbConnection\CacheManager;

abstract class CacheableModel extends \Hyperf\Database\Model\Model implements CacheableInterface
{
    use Cacheable;

    /**
     * Delete model from cache.
     */
    public function deleteCache($refresh = true): bool
    {
        $manager = CacheManager::getInstance();

        if ($refresh) {
            $manager->updateCache($this);
            return true;
        }

        return $manager->destroy([$this->getKey()], get_called_class());
    }

    /**
     * refresh model from cache.
     * @return static
     */
    public function refreshFromCache()
    {
        return CacheManager::getInstance()->reloadCache($this);
    }
}

