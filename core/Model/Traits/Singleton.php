<?php
namespace Whyperf\Model\Traits;

/**
 * Trait Singleton
 * @package whyperf\Model\Traits
 */
trait Singleton
{

    private static $instance;

    /**
     * @param ...$args
     * @return static
     * @author WANG RUNXIN
     */
    static function getInstance(...$args)
    {
        if (! isset(self::$instance)) {
            self::$instance = new static(...$args);
        }
        return self::$instance;
    }
}

?>