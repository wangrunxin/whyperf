<?php
namespace Whyperf\Model\Traits;

use Swoole\Coroutine;

/**
 * Trait CoroutineSafeSingleton
 * @package whyperf\Model\Traits
 * @author WANG RUNXIN
 */
trait CoroutineSafeSingleton
{

    private static $instance = [];

    private $destroyed = false;

    static function getInstance(...$args)
    {
        $cid = Coroutine::getCid();

        if (! isset(self::$instance[$cid])) {
            self::$instance[$cid] = new static(...$args);
            if ($cid > 0) {
                Coroutine::defer(function () use ($cid) {
                    $core = self::$instance[$cid];
                    if (is_object($core)) {
                        if (method_exists($core, 'before_destroy')) {
                            $core->before_destroy();
                        }
                    }
                    unset($core);
                    unset(self::$instance[$cid]);
                });
            }
        }
        return self::$instance[$cid];
    }

    static function hasInstance(): bool
    {
        $cid = Coroutine::getCid();
        if ($cid > 0) {
            return isset(self::$instance[$cid]);
        }
        return false;
    }

    static function getCoroutineId(): string
    {
        return Coroutine::getCid();
    }

    static function setInstance($instance)
    {
        $cid = Coroutine::getCid();
        self::$instance[$cid] = $instance;

        if ($cid > 0) {
            Coroutine::defer(function () use ($cid) {
                unset(self::$instance[$cid]);
            });
        }

        return self::$instance[$cid];
    }

    function before_destroy()
    {
        if($this->destroyed){
            return;
        }

        $this->destroyed = true;

        //do something;
    }

    function destroy(int $cid = null)
    {
        if ($cid === null) {
            $cid = Coroutine::getCid();
        }

        $core = self::$instance[$cid];
        if (is_object($core)) {
            if (method_exists($core, before_destroy)) {
                $core->before_destroy();
            }
        }
        unset($core);
        unset(self::$instance[$cid]);
    }
}

?>