<?php

namespace Whyperf\Rpc;

use App\App;
use Whyperf\Model\Traits\Singleton;
use Whyperf\Rpc\Annotation\AbstractMicroService;
use Whyperf\Rpc\Traits\PhpFileTraits;
use Whyperf\Whyperf;

/**
 * Class ClientInterfaceDi
 * @package whyperf\Rpc
 * @author WANG RUNXIN
 */
class ClientInterfaceDi
{

    use Singleton;
    use PhpFileTraits;

    private $load = false;

    private $interfaces = [];

    private $clients = [];

    private $interfacesWithClient = [];

    private $interfacesWithoutClient = [];

    function __construct()
    {
        $this->scanInterfaces();
    }

    private function getRpcDir(): array
    {
        $dir = [];

        foreach (App::getServiceList() as $service) {
            /**
             * @var AbstractMicroService $service
             */
            $dir[] = $service::getRootDir();
        }

        return $dir;
    }

    function getInterfacesWithClient(): array
    {
        if (!$this->load) {
            $this->scanInterfaces();
        }
        return $this->interfacesWithClient;
    }

    function getInterfacesWithoutClient(): array
    {
        if (!$this->load) {
            $this->scanInterfaces();
        }
        return $this->interfacesWithoutClient;
    }

    public function scanInterfaces()
    {
        if ($this->load) {
            return;
        }

        $this->load = true;

        $interfaces = [];

        $this->loadRoot($this->getRpcDir(), function ($file, $path) use (&$interfaces) {
            $class = $this->getFullClass($file, self::$FILE_TYPE_INTERFACE);
            if ($class === false) {
               return;
            }
            $interfaces[$path][] = $class;
        });

        $this->interfaces = $interfaces;

        if (count($this->interfaces) == 0) {
            return;
        }

        $this->clients = $this->scanClients();

        if (count($this->clients) == 0) {
            return;
        }

        $this->clientDi();
    }


    private function scanClients(): array
    {
        $clients = [];

        $this->loadRoot($this->getRpcDir(), function ($file, $path) use (&$clients) {
            $class = $this->getFullClass($file);
            if ($class === false) {
                return;
            }
            $clients[$path][] = $class;
        });

        return $clients;
    }

    private function clientDi()
    {
        $interfacesWithClient = [];
        $interfacesWithoutClient = [];

        $interfaces = $this->interfaces;
        $clients = $this->clients;
        $services = [];



        foreach ($clients as $path => $clientList) {
            foreach ($clientList as $client) {
                if (is_subclass_of($client, AbstractMicroService::class)) {
                    if ($client::isDisabled()) {
                        unset($clients[$path]);
                        unset($interfacesWithoutClient[$client]);
                        unset($services[$path]);
                        break;
                    }
                    $interfacesWithoutClient[$client] = $interfaces[$path] ?? [];
                    $services[$path] = $client;
                    break;
                }
            }
        }

        foreach ($clients as $path => $clientList) {

            if (!isset($interfaces[$path])) {
                continue;
            }

            foreach ($clientList as $key => $client) {
                if (is_subclass_of($client, AbstractMicroService::class)) {
                    continue;
                }

                $implements = class_implements($client);
                if (!isset($interfaces[$path]) || !is_array($interfaces[$path])) {
                    continue;
                }

                foreach ($interfaces[$path] as $key => $interface) {
                    if (in_array($interface, $implements)) {
                        unset($interfacesWithoutClient[$services[$path]][$key]);
                        $interfacesWithClient[$services[$path]][] = $interface;
                        Whyperf::getContainer()->define($interface, $client);
                    }
                }
            }
        }

        foreach ($interfacesWithoutClient as $client => $data) {
            if (count($data) == 0) {
                unset($interfacesWithoutClient[$client]);
            }
        }

        $this->interfacesWithClient = $interfacesWithClient;
        $this->interfacesWithoutClient = $interfacesWithoutClient;
    }
}