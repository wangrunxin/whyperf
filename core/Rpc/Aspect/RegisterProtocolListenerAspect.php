<?php


namespace Whyperf\Rpc\Aspect;


use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\Dispatcher\AbstractRequestHandler;
use Hyperf\JsonRpc\DataFormatter;
use Hyperf\JsonRpc\JsonRpcHttpTransporter;
use Hyperf\JsonRpc\JsonRpcPoolTransporter;
use Hyperf\JsonRpc\Packer\JsonEofPacker;
use Hyperf\JsonRpc\Packer\JsonLengthPacker;
use Hyperf\JsonRpc\PathGenerator;
use Hyperf\Rpc\ProtocolManager;
use Hyperf\Utils\Packer\JsonPacker;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Whyperf\Audit\Audit;
use Whyperf\Database\QueryBuilder;
use Whyperf\MultiTenant\MultiTenantAuthenticator;
use Whyperf\Rpc\ConsumerClient;

use Whyperf\Rpc\ServiceServer;
use Whyperf\System\CoroutineEnv\CoreGo;
use Whyperf\Whyperf;

/**
 * @Aspect(
 *   classes={
 *     "Hyperf\JsonRpc\Listener\RegisterProtocolListener::process"
 *   }
 * )
 */
class RegisterProtocolListenerAspect extends AbstractAspect
{
    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $protocolManager = Whyperf::getContainer()->get(ProtocolManager::class);

        $protocolManager->register('jsonrpc', [
            'packer' => JsonEofPacker::class,
            'transporter' => JsonRpcPoolTransporter::class,
            'path-generator' => PathGenerator::class,
            'data-formatter' => DataFormatter::class,
        ]);

        $protocolManager->register('jsonrpc-tcp-length-check', [
            'packer' => JsonLengthPacker::class,
            'transporter' => JsonRpcPoolTransporter::class,
            'path-generator' => PathGenerator::class,
            'data-formatter' => DataFormatter::class,
        ]);

        $protocolManager->register('jsonrpc-http', [
            'packer' => JsonPacker::class,
            'transporter' => JsonRpcHttpTransporter::class,
            'path-generator' => PathGenerator::class,
            'data-formatter' => DataFormatter::class,
        ]);
    }
}
