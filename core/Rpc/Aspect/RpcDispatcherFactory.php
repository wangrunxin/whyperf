<?php


namespace Whyperf\Rpc\Aspect;


use Hyperf\Di\Annotation\AnnotationCollector;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\Di\ReflectionManager;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\MiddlewareManager;
use Hyperf\Rpc\Contract\PathGeneratorInterface;
use Hyperf\RpcServer\Event\AfterPathRegister;
use Hyperf\RpcServer\Router\DispatcherFactory;
use Hyperf\Utils\Str;
use Psr\EventDispatcher\EventDispatcherInterface;
use Whyperf\Rpc\Annotation\AbstractMicroService;

/**
 * @\Hyperf\Di\Annotation\Aspect(
 *   classes={
 *     "Hyperf\RpcServer\Router\DispatcherFactory::__construct"
 *   }
 * )
 */
class RpcDispatcherFactory extends AbstractAspect
{
    protected DispatcherFactory $factory;

    protected EventDispatcherInterface $eventDispatcher;

    protected PathGeneratorInterface $pathGenerator;

    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        [$eventDispatcher, $pathGenerator] = $proceedingJoinPoint->getArguments();
        $proceedingJoinPoint->process();
        $this->factory = $proceedingJoinPoint->getInstance();
        $this->eventDispatcher = $eventDispatcher;
        $this->pathGenerator = $pathGenerator;
        $this->initMsAnnotationRoute(AnnotationCollector::list());
    }

    private function initMsAnnotationRoute(array $collector): void
    {
        foreach ($collector as $className => $metadata) {
            if (!isset($metadata['_c']) || !is_array($metadata['_c'])) {
                continue;
            }

            foreach ($metadata['_c'] as $key => $obj) {
                if ($obj instanceof AbstractMicroService) {
                    $middlewares = $this->handleMiddleware($metadata['_c']);
                    try {
                        $this->handleRpcService($className, $obj, $metadata['_m'] ?? [], $middlewares);
                    } catch (\Throwable $throwable) {
                        var_dump($throwable->getMessage());
                    }
                }
            }
        }
    }

    private function handleMiddleware(array $metadata): array
    {
        $hasMiddlewares = isset($metadata[Middlewares::class]);
        $hasMiddleware = isset($metadata[Middleware::class]);
        if (!$hasMiddlewares && !$hasMiddleware) {
            return [];
        }
        if ($hasMiddlewares && $hasMiddleware) {
            throw new ConflictAnnotationException('Could not use @Middlewares and @Middleware annotation at the same times at same level.');
        }
        if ($hasMiddlewares) {
            // @Middlewares
            /** @var Middlewares $middlewares */
            $middlewares = $metadata[Middlewares::class];
            $result = [];
            foreach ($middlewares->middlewares as $middleware) {
                $result[] = $middleware->middleware;
            }
            return $result;
        }
        // @Middleware
        /** @var Middleware $middleware */
        $middleware = $metadata[Middleware::class];
        return [$middleware->middleware];
    }

    /**
     * Register route according to RpcService annotation.
     */
    private function handleRpcService(
        string $className,
        AbstractMicroService $annotation,
        array $methodMetadata,
        array $middlewares = []
    ): void
    {
        $prefix = $annotation->name ?: $className;

        $router = $this->factory->getRouter($annotation->server);

        $publicMethods = ReflectionManager::reflectClass($className)->getMethods(\ReflectionMethod::IS_PUBLIC);

        foreach ($publicMethods as $reflectionMethod) {
            try {
                $methodName = $reflectionMethod->getName();
                if (Str::startsWith($methodName, '__')) {
                    continue;
                }
                $path = $this->pathGenerator->generate($prefix, $methodName);

                $router->addRoute($path, [
                    $className,
                    $methodName,
                ]);

                $methodMiddlewares = $middlewares;
                // Handle method level middlewares.
                if (isset($methodMetadata[$methodName])) {
                    $methodMiddlewares = array_merge($this->handleMiddleware($methodMetadata[$methodName]), $middlewares);
                }
                // TODO: Remove array_unique from v3.0.
                $methodMiddlewares = array_unique($methodMiddlewares);

                // Register middlewares.
                MiddlewareManager::addMiddlewares($annotation->server, $path, 'POST', $methodMiddlewares);

                // Trigger the AfterPathRegister event.
                $this->eventDispatcher->dispatch(new AfterPathRegister($path, $className, $methodName, $annotation));
            } catch (\Throwable $throwable) {
                var_dump($throwable->getMessage());
            }
        }
    }
}
