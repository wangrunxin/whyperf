<?php


namespace Whyperf\Rpc\Aspect;

use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @\Hyperf\Di\Annotation\Aspect(
 *   classes={
 *     "Hyperf\JsonRpc\TcpServer::buildJsonRpcRequest"
 *   }
 * )
 */
class TcpServerWithHeaderAspect extends AbstractAspect
{
    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        [$fd, $fromId, $data] = $proceedingJoinPoint->getArguments();

        /**
         * @var ServerRequestInterface $request
         */
        $request = $proceedingJoinPoint->process();

        if(!isset($data["header"]) || !is_array($data["header"])){
            return $request;
        }

        foreach ($data["header"] as $key => $val){
            $request = $request->withHeader($key, $val);
        }

        return $request;
    }
}
