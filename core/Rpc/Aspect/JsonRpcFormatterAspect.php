<?php


namespace Whyperf\Rpc\Aspect;


use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\Dispatcher\AbstractRequestHandler;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\MiddlewareInterface;
use Whyperf\Audit\Audit;
use Whyperf\Database\QueryBuilder;
use Whyperf\MultiTenant\MultiTenantAuthenticator;
use Whyperf\Rpc\ConsumerClient;

use Whyperf\Rpc\ServiceServer;
use Whyperf\System\CoroutineEnv\CoreGo;

/**
 * @Aspect(
 *   classes={
 *     "Hyperf\JsonRpc\DataFormatter::formatRequest"
 *   }
 * )
 */
class JsonRpcFormatterAspect extends AbstractAspect
{
    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        $json = $proceedingJoinPoint->process();

        foreach (CoreGo::getCoreGo()->getComponents() as $component){
            $component->attachedJsonRpc($json);
        }

        return $json;
    }
}
