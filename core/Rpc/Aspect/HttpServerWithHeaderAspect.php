<?php


namespace Whyperf\Rpc\Aspect;


use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\Dispatcher\AbstractRequestHandler;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Whyperf\Audit\Audit;
use Whyperf\Database\QueryBuilder;
use Whyperf\Rpc\ConsumerClient;

use Whyperf\Rpc\ServiceServer;
use Whyperf\System\CoroutineEnv\CoreGo;

/**
 * @Aspect(
 *   classes={
 *     "Hyperf\JsonRpc\HttpServer::initRequestAndResponse"
 *   }
 * )
 */
class HttpServerWithHeaderAspect extends AbstractAspect
{
    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {

        /**
         * @var ServerRequestInterface $request
         */
        [$request, $psr7Response] = $proceedingJoinPoint->process();

        $data = $request->getAttribute("data");

        if (!isset($data["header"]) || !is_array($data["header"])) {
            return [$request, $psr7Response];
        }

        foreach ($data["header"] as $key => $val) {
            $request = $request->withHeader($key, $val);
        }

        return [$request, $psr7Response];
    }
}
