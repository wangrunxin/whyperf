<?php

namespace Whyperf\Rpc;

use Hyperf\Rpc\ProtocolManager;

/**
 * Class QueueRpcProtocolManager
 * @package whyperf\Rpc
 * @author WANG RUNXIN
 */
class QueueRpcProtocolManager extends ProtocolManager
{
    public function getTransporter(string $name): string
    {
        return QueueTransporter::class;
    }
}