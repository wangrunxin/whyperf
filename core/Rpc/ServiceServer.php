<?php

namespace Whyperf\Rpc;

use App\App;
use Hyperf\Contract\StdoutLoggerInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;
use Whyperf\Rpc\Annotation\AbstractMicroService;
use Whyperf\Rpc\Traits\PhpFileTraits;
use Whyperf\Whyperf;

/**
 * Class ServiceServer
 * @package whyperf\Rpc
 * @author WANG RUNXIN
 */
class ServiceServer
{
    use PhpFileTraits;

    static function getConfig(): array
    {
        $server = new self();
        return $server->scanServers();
    }

    private function getRpcDir(): array
    {
        return App::getRpcDir();
    }

    private function getServiceDir(): array
    {
        return App::getServiceDir();
    }

    function scanServers(): array
    {
        $servers = [];
        $io = new SymfonyStyle(new ArrayInput([]), new ConsoleOutput());
        $io->note(sprintf("scan %s for jsonRpc service", implode(", ", $this->getRpcDir())));
        $this->loadRoot($this->getRpcDir(), function ($file) use (&$servers) {
            $class = $this->getFullClass($file);
            if ($class === false) {
                return;
            }
            $servers[] = $class;
        });

        if (count($servers) == 0) {
            return [];
        }

        $this->loadService($servers);

        return $this->readConfig($servers);
    }

    private function loadService()
    {

        $services = [];

        $this->loadRoot($this->getServiceDir(), function ($file) use (&$services) {
            $class = $this->getFullClass($file);
            $r = new \ReflectionClass($class);
            $services[$file] = $r->getDocComment();
        });

        return $services;
    }

    private function readConfig($servers): array
    {
        $config = [];

        $loaded = [];

        $services = $this->loadService();

        foreach ($servers as $server) {
            if (is_subclass_of($server, AbstractMicroService::class)) {
                /**
                 * @var AbstractMicroService $server
                 */
                if ($server::isDisabled() || !$this->hasService($server, $services)) {
                    continue;
                }

                QueueServers::getInstance()->addServer($server::getName(), $server::getServerClass());

                $loaded[] = [$server::getServerName(), $server::getServerBref()];

                $config[] = $server::getServerConfig();
            }
        }

        $io = new SymfonyStyle(new ArrayInput([]), new ConsoleOutput());
        $io->table(["as service", "info"], $loaded);

        return $config;
    }

    private function hasService($server, $services): bool
    {

        $target = sprintf("@\%s()", $server);

        foreach ($services as $service) {
            if (strpos($service, $target) !== false) {
                return true;
            }
        }

        return false;
    }

    private function getService($server, $services)
    {


        $target = sprintf("@\%s()", $server);

        foreach ($services as $service) {
            if (strpos($service, $target) !== false) {
                return $service;
            }
        }

        return false;
    }

    /**
     * @param array $configs
     * @return array
     */
    static function addToConfig(array $configs): array
    {
        foreach ($configs as $key => $config) {
            if (!isset($config["server"]["servers"]) || !is_array($config["server"]["servers"])) {
                continue;
            }

            $config["server"]["servers"] = array_merge($config["server"]["servers"], self::getConfig());
            $configs[$key] = $config;
            break;
        }

        return $configs;
    }
}