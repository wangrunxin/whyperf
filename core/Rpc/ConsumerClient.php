<?php

namespace Whyperf\Rpc;

use App\App;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;
use Whyperf\Rpc\Annotation\AbstractMicroService;
use Whyperf\Rpc\Traits\PhpFileTraits;

/**
 * Class ConsumerClient
 * @package whyperf\Rpc
 * @author WANG RUNXIN
 */
class ConsumerClient
{
    use PhpFileTraits;

    private $record = [];

    static function getConfig(): array
    {
        $server = new self();
        return $server->scanServers();
    }

    private function getRpcDir(): array
    {
        $dir = [];

        foreach (App::getServiceList() as $service){
            /**
             * @var AbstractMicroService $service
             */
            $dir[] = $service::getRootDir();
        }

        return $dir;
    }

    function scanServers(): array
    {
        $config = array_merge(
            $this->scanInterfaceWithClient(),
            $this->scanInterfaceWithoutClient()
        );

        $io = new SymfonyStyle(new ArrayInput([]), new ConsoleOutput());
        $io->table(["as consumer", "protocol", "service host"], $this->record);

        return $config;
    }

    function scanInterfaceWithClient(): array
    {
        return $this->readConfig(ClientInterfaceDi::getInstance()->getInterfacesWithClient(), true);
    }

    function scanInterfaceWithoutClient(): array
    {
        return $this->readConfig(ClientInterfaceDi::getInstance()->getInterfacesWithoutClient());
    }


    private function readConfig($servers, $hasClient = false): array
    {
        $config = [];

        foreach ($servers as $server => $interfaces) {
            /**
             * @var AbstractMicroService $server
             */
            if (is_array($interfaces)) {
                foreach ($interfaces as $interface) {
                    $clientConfig = $server::getClientConfig($interface, $hasClient);
                    $this->record[$clientConfig["name"]] = [
                        $clientConfig["name"],
                        $clientConfig["protocol"],
                        $this->getServiceHostInfo($clientConfig)
                    ];

                    $config[] = $clientConfig;
                }
            }
        }

        return $config;
    }

    private function getServiceHostInfo($clientConfig)
    {
        $info = [];

        if (isset($clientConfig["registry"])) {
            $info = $clientConfig["registry"];
        }
        if (isset($clientConfig["nodes"])) {
            $info = $clientConfig["nodes"];
        }

        return json_encode($info);
    }

    /**
     * @param array $configs
     * @return array
     */
    static function addToConfig(array $configs): array
    {
        foreach ($configs as $key => $config) {
            if (!isset($config["services"]["consumers"]) || !is_array($config["services"]["consumers"])) {
                continue;
            }

            $config["services"]["consumers"] = array_merge($config["services"]["consumers"], self::getConfig());
            $configs[$key] = $config;
        }

        return $configs;
    }

}