<?php

namespace Whyperf\Rpc\Annotation;

use App\JsonRpc\Demo\Interfaces\CalculatorServiceInterface;
use Hyperf\Contract\OnReceiveInterface;
use Hyperf\JsonRpc\JsonRpcHttpTransporter;
use Hyperf\RpcServer\Annotation\RpcService;
use Hyperf\Server\Event;
use Hyperf\Server\Server;
use Swoole\Coroutine\Server\Connection;
use Swoole\Server as SwooleServer;
use Whyperf\Http\JsonRpcServerWithoutWorker;
use Whyperf\Rpc\Model\AbstractRpcConsumer;
use Whyperf\Rpc\Server\RpcServer;
use Whyperf\Rpc\Server\TcpServer;
use Whyperf\Whyperf;

/**
 * Class AbstractMicroService
 * @package whyperf\Rpc\Annotation
 * @author WANG RUNXIN
 */
abstract class AbstractMicroService extends RpcService
{
    const PROTOCOL_TCP = "jsonrpc";
    const PROTOCOL_HTTP = "jsonrpc-http";

    /**
     * @var string
     */
    public $name = '';

    /**
     * @var string
     */
    public $server;

    /**
     * @var string
     */
    public $protocol;

    /**
     * @var string
     */
    public $publishTo = '';

    abstract function getRegistry(): array;

    abstract function getPort(): int;

    abstract function getHostForConsumer():string;

    abstract static function getServerClass(): string;

    /**
     * @return array
     * @example return [["host"=>"127.0.0.1", "port" => 9503]];
     */
    abstract function getHostList(): array;

    public function __construct($value = [])
    {
        $this->server = $this->getName();
        $this->name = $this->getName();
        $this->protocol = $this->getProtocol();
        $this->publishTo = $this->getPublishTo();

        parent::__construct($value);
    }

    static function getName(): string
    {
        return static::class;
    }

    function getHost(): string{
        return "127.0.0.1";
    }

    function getType(): int
    {
        switch ($this->getProtocol()) {
            case self::PROTOCOL_TCP:
                return Server::SERVER_BASE;
                break;
            default:
                return Server::SERVER_HTTP;
                break;
        }
    }

    static function isDisabled(): bool
    {
        return false;
    }

    function getProtocol(): string
    {
        return self::PROTOCOL_TCP;
    }

    function getPublishTo(): string
    {
        return '';
    }

    function getCallBacks(): array
    {
        switch ($this->getProtocol()) {
            case self::PROTOCOL_HTTP:
                return $this->getHttpCallBacks();
            case self::PROTOCOL_TCP:
                return $this->getTcpCallBacks();
            default:
                return [];
        }
    }

    protected function getHttpCallBacks(): array
    {
        return [
            Event::ON_REQUEST => [static::getServerClass(), 'onRequest'],
        ];
    }

    protected function getTcpCallBacks(): array
    {
        return [
            Event::ON_RECEIVE => [static::getServerClass(), 'onReceive'],
        ];
    }

    function getSockType(): int
    {
        return SWOOLE_SOCK_TCP;
    }

    function getSettings(): array
    {
        switch ($this->getProtocol()) {
            case self::PROTOCOL_TCP:
                return [
                    'open_eof_split' => true,
                    'package_eof' => "\r\n",
                    'package_max_length' => 1024 * 1024 * 2
                ];
                break;
            default:
                return [];
                break;
        }

    }

    static function getServerConfig(): array
    {
        $model = new static();
        return [
            'name' => $model->getName(),
            'type' => $model->getType(),
            'host' => $model->getHost(),
            'port' => $model->getPort(),
            'sock_type' => $model->getSockType(),
            'callbacks' => $model->getCallBacks(),
            'settings' => $model->getSettings()
        ];
    }

    static function getServerBref(): string
    {
        $model = new static();
        return json_encode([
            'host' => $model->getHost(),
            'port' => $model->getPort(),
            'protocol' => $model->getProtocol(),
        ]);
    }

    static function getServerName(): string
    {
        $model = new static();
        return $model->getName();
    }

    function addServiceHost(array $config): array
    {
        if (count($this->getRegistry()) != 0) {
            $config["registry"] = $this->getRegistry();
        } else {
            $config['nodes'] = $this->getServiceNodes();
        }

        return $config;
    }

    function getServiceNodes(): array
    {
        if (count($this->getHostList()) == 0) {
            return [
                ['host' => $this->getHostForConsumer(), 'port' => $this->getPort()],
            ];
        }

        return $this->getHostList();
    }

    function getExchange(): string
    {
        return "whyperf";
    }

    function getRoutingKey(string $method = "*"): string
    {
        return $this->getRoutingKeyByScenario($this->getQueueTagByScenario($this->getScenarioByMethod($method)));
    }

    function getRoutingKeyByScenario(string $scenario)
    {
        return get_class($this) . "." . $scenario;
    }

    function getScenarioByMethod(string $method): string
    {
        return "default";
    }

    function getQueueTagByScenario(string $scenario): string
    {
        return $scenario;
    }

    function getQueue(string $method = "#"): string
    {
        return $this->getQueueByScenario($this->getQueueTagByScenario($this->getScenarioByMethod($method)), $method);
    }

    function getQueueByScenario(string $scenario, string $method = "#"): string
    {
        return get_class($this) . "." . $scenario . "." . $method;
    }

    function getSimpleClientConfig(): array
    {
        $config = [
            'name' => $this->getName(),
            'protocol' => $this->getProtocol(),
            'options' => [
                'classname' => get_class($this)
            ]
        ];

        return $this->addServiceHost($config);
    }

    function getFullClientConfig($interface): array
    {
        $config = [
            // name 需与服务提供者的 name 属性相同
            'name' => $this->getName(),
            'service' => $interface,
            // 服务提供者的服务协议，可选，默认值为 jsonrpc-http
            'protocol' => $this->getProtocol(),
            // 负载均衡算法，可选，默认值为 random
            'load_balancer' => 'random',
            // 配置项，会影响到 Packer 和 Transporter
            'options' => $this->readConfig("options", [
                'connect_timeout' => 5.0,
                'recv_timeout' => 5.0,
                'settings' => [
                    // 根据协议不同，区分配置
                    'open_eof_split' => true,
                    'package_eof' => "\r\n",
                    // 'open_length_check' => true,
                    // 'package_length_type' => 'N',
                    // 'package_length_offset' => 0,
                    // 'package_body_offset' => 4,
                ],
                // 重试次数，默认值为 2，收包超时不进行重试。暂只支持 JsonRpcPoolTransporter
                'retry_count' => 2,
                // 重试间隔，毫秒
                'retry_interval' => 100,
                // 当使用 JsonRpcPoolTransporter 时会用到以下配置
                'pool' => [
                    'min_connections' => 1,
                    'max_connections' => 32,
                    'connect_timeout' => 10.0,
                    'wait_timeout' => 3.0,
                    'heartbeat' => -1,
                    'max_idle_time' => 60.0,
                ],
            ]),
        ];

        return $this->addServiceHost($config);
    }

    protected function readConfig($name, $default){
        $path = BASE_PATH . "/config/jsonrpc.php";
        if(file_exists($path)){
            $config = require $path;
            if(is_array($config)){
                return $config[$name] ?? $default;
            }
        }

        return $default;
    }

    static function getClientConfig($interface, $usingClientModel): array
    {
        $model = new static();

        if ($usingClientModel) {
            return $model->getSimpleClientConfig();
        }

        return $model->getFullClientConfig($interface);
    }

    static function getRootDir()
    {
        return dirname((new \ReflectionClass(static::class))->getFileName());
    }
}