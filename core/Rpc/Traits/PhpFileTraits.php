<?php

namespace Whyperf\Rpc\Traits;

/**
 * Trait PhpFileTraits
 * @package whyperf\Rpc\Traits
 */
trait PhpFileTraits
{

    protected static $FILE_TYPE_CLASS = "class";
    protected static $FILE_TYPE_INTERFACE = "interface";
    protected static $FILE_TYPE_ABSTRACT = "abstract";

    private function getFullClass($file, $type = "class")
    {
        $namespace = $this->getClassNamespaceFromFile($file);

        switch ($type) {
            case self::$FILE_TYPE_CLASS:
                $className = $this->getClassNameFromFile($file);
                break;
            case self::$FILE_TYPE_INTERFACE:
                $className = $this->getInterfaceFromFile($file);
                break;
            case self::$FILE_TYPE_ABSTRACT:
                $className = $this->getAbstractClassFromFile($file);
                break;
        }

        if (is_null($namespace) || is_null($className)) {
            return false;
        }

        return $namespace . "\\" . $className;
    }

    private function loadRoot(array $rootDirs, callable $handler)
    {
        $files = [];

        foreach ($rootDirs as $rootDir) {
            if (!is_dir($rootDir)) {
                continue;
            }
            if ($handle = opendir($rootDir)) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != "..") {
                        $path = $rootDir . DIRECTORY_SEPARATOR . $entry;
                        if (is_dir($path)) {
                            $this->load($path, $handler, $rootDir);
                        } else {
                            $handler($path, $rootDir);
                        }
                    }
                }

                closedir($handle);
            }
        }
    }

    private function load($rootDir, callable $handler, $root)
    {
        $files = [];
        if ($handle = opendir($rootDir)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    $path = $rootDir . DIRECTORY_SEPARATOR . $entry;
                    if (is_dir($path)) {
                        $this->load($path, $handler, $root);
                    } else {
                        $handler($path, $root);
                    }
                }
            }

            closedir($handle);
        }
    }


    /**
     * get the class namespace form file path using token
     *
     * @param $filePathName
     *
     * @return  null|string
     */
    protected function getClassNamespaceFromFile($filePathName)
    {
        $src = file_get_contents($filePathName);

        $tokens = token_get_all($src);
        $count = count($tokens);
        $i = 0;
        $namespace = '';
        $namespace_ok = false;
        while ($i < $count) {
            $token = $tokens[$i];
            if (is_array($token) && $token[0] === T_NAMESPACE) {
                // Found namespace declaration
                while (++$i < $count) {
                    if ($tokens[$i] === ';') {
                        $namespace_ok = true;
                        $namespace = trim($namespace);
                        break;
                    }
                    $namespace .= is_array($tokens[$i]) ? $tokens[$i][1] : $tokens[$i];
                }
                break;
            }
            $i++;
        }
        if (!$namespace_ok) {
            return null;
        } else {
            return $namespace;
        }
    }

    /**
     * get the class name form file path using token
     *
     * @param $filePathName
     *
     * @return  mixed
     */
    protected function getClassNameFromFile($filePathName)
    {
        $php_code = file_get_contents($filePathName);

        $classes = array();
        $tokens = token_get_all($php_code);
        $count = count($tokens);
        for ($i = 2; $i < $count; $i++) {
            if ($tokens[$i - 2][0] == T_CLASS
                && $tokens[$i - 1][0] == T_WHITESPACE
                && $tokens[$i][0] == T_STRING
            ) {

                $class_name = $tokens[$i][1];
                $classes[] = $class_name;
                break;
            }
        }
        return isset($classes[0]) ? $classes[0] : null;
    }

    /**
     * get the class name form file path using token
     *
     * @param $filePathName
     *
     * @return  mixed
     */
    protected function getInterfaceFromFile($filePathName)
    {
        $php_code = file_get_contents($filePathName);

        $classes = array();
        $tokens = token_get_all($php_code);
        $count = count($tokens);
        for ($i = 2; $i < $count; $i++) {
            if ($tokens[$i - 2][0] == T_INTERFACE
                && $tokens[$i - 1][0] == T_WHITESPACE
                && $tokens[$i][0] == T_STRING
            ) {
                $class_name = $tokens[$i][1];
                $classes[] = $class_name;
                break;
            }
        }

        return isset($classes[0]) ? $classes[0] : null;
    }

    /**
     * get the class name form file path using token
     *
     * @param $filePathName
     *
     * @return  mixed
     */
    protected function getAbstractClassFromFile($filePathName)
    {
        $php_code = file_get_contents($filePathName);

        $classes = array();
        $tokens = token_get_all($php_code);
        $count = count($tokens);

        for ($i = 4; $i < $count; $i++) {
            if ($tokens[$i - 4][0] == T_ABSTRACT
                && $tokens[$i - 3][0] == T_WHITESPACE
                && $tokens[$i - 2][0] == T_CLASS
                && $tokens[$i - 1][0] == T_WHITESPACE
                && $tokens[$i][0] == T_STRING
            ) {
                $class_name = $tokens[$i][1];
                $classes[] = $class_name;
                break;
            }
        }

        return isset($classes[0]) ? $classes[0] : null;
    }
}