<?php

namespace Whyperf\Rpc\Traits;

use Whyperf\Rpc\Annotation\AbstractMicroService;
use Whyperf\Rpc\Model\AbstractRpcClient;
use Whyperf\Rpc\Model\AbstractRpcConsumer;

/**
 * Trait DynamicQueueTrait
 * @package Whyperf\Rpc\Traits
 * @author WANG RUNXIN
 */
trait DynamicQueueTrait
{

    function getExchange(): string
    {
        return $this->getTargetMicroService()->getExchange();
    }

    function getQueue(string $methodName): string
    {
        return $this->getTargetMicroService()->getQueue($methodName);
    }

    function getRoutingKey(string $methodName): string
    {
        return $this->getTargetMicroService()->getRoutingKey($methodName);
    }

    protected function getTargetMicroService(): AbstractMicroService
    {
        if ($this->isAbstractRpcClient()) {
            return $this->getService();
        }

        $clientClassname = $this->serviceName;
        return new $clientClassname();
    }

    function formatData(array $data, string $methodName): array
    {
        $data["routingkey"] = $this->getRoutingKey($methodName);
        $data["exchange"] = $this->getExchange();
        $data["queue"] = $this->getQueue($methodName);
        $data[AbstractRpcConsumer::SERVICE_CLASSNAME] = $this->getTargetMicroService()->getName();

        return $data;
    }

    protected function isAbstractRpcClient(): bool
    {
        if ($this instanceof AbstractRpcClient) {
            return true;
        }
        return false;
    }
}