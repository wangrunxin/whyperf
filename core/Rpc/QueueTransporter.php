<?php

namespace Whyperf\Rpc;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Hyperf\Amqp\Message\DynamicRpcMessage;
use Hyperf\Amqp\RpcClient;
use Hyperf\Guzzle\ClientFactory;
use Hyperf\JsonRpc\JsonRpcHttpTransporter;
use Hyperf\LoadBalancer\LoadBalancerInterface;
use Hyperf\LoadBalancer\Node;
use Hyperf\Rpc\Contract\TransporterInterface;
use Hyperf\Rpc\ProtocolManager;
use Hyperf\Utils\ApplicationContext;
use Whyperf\Rpc\Model\DynamicRpcQueueMessage;

/**
 * Class QueueTransporter
 * @package whyperf\Rpc
 * @author WANG RUNXIN
 */
class QueueTransporter implements TransporterInterface
{
    protected $data;
    /**
     * @var null|LoadBalancerInterface
     */
    private $loadBalancer;

    /**
     * If $loadBalancer is null, will select a node in $nodes to request,
     * otherwise, use the nodes in $loadBalancer.
     *
     * @var Node[]
     */
    private $nodes = [];

    /**
     * @var float
     */
    private $connectTimeout = 5;

    /**
     * @var float
     */
    private $recvTimeout = 5;

    /**
     * @var \Hyperf\Guzzle\ClientFactory
     */
    private $clientFactory;

    /**
     * @var array
     */
    private $clientOptions;

    public function __construct(ClientFactory $clientFactory, array $config = [])
    {
        $this->clientFactory = $clientFactory;
        if (!isset($config['recv_timeout'])) {
            $config['recv_timeout'] = $this->recvTimeout;
        }
        if (!isset($config['connect_timeout'])) {
            $config['connect_timeout'] = $this->connectTimeout;
        }
        $this->clientOptions = $config;
    }

    public function send(string $data)
    {
        $this->data = json_decode($data, true);
        $rpcClient = ApplicationContext::getContainer()->get(RpcClient::class);

        var_dump([
            $this->getExchange(), $this->getRoutingKey(), $this->getQueue(), $this->getFullData()
        ]);

        return $rpcClient->call(new DynamicRpcQueueMessage($this->getExchange(), $this->getRoutingKey(), $this->getFullData(), $this->getQueue()));
    }

    function getExchange()
    {
        return $this->data["exchange"];
    }

    function getRoutingKey()
    {
        return $this->data["routingkey"];
    }

    function getQueue()
    {
        return $this->data["queue"];
    }

    function getFullData()
    {
        return json_encode(array_merge($this->data, ["port" => $this->getNode()->port]));
    }

    public function recv()
    {
        throw new \RuntimeException(__CLASS__ . ' does not support recv method.');
    }

    public function getClient(): Client
    {
        $clientOptions = $this->clientOptions;
        // Swoole HTTP Client cannot set recv_timeout and connect_timeout options, use timeout.
        $clientOptions['timeout'] = $clientOptions['recv_timeout'] + $clientOptions['connect_timeout'];
        unset($clientOptions['recv_timeout'], $clientOptions['connect_timeout']);
        return $this->clientFactory->create($clientOptions);
    }

    public function getLoadBalancer(): ?LoadBalancerInterface
    {
        return $this->loadBalancer;
    }

    public function setLoadBalancer(LoadBalancerInterface $loadBalancer): TransporterInterface
    {
        $this->loadBalancer = $loadBalancer;
        return $this;
    }

    /**
     * @param \Hyperf\LoadBalancer\Node[] $nodes
     */
    public function setNodes(array $nodes): self
    {
        $this->nodes = $nodes;
        return $this;
    }

    public function getNodes(): array
    {
        return $this->nodes;
    }

    public function getClientOptions(): array
    {
        return $this->clientOptions;
    }

    private function getEof()
    {
        return "\r\n";
    }

    /**
     * If the load balancer is exists, then the node will select by the load balancer,
     * otherwise will get a random node.
     */
    private function getNode(): Node
    {
        if ($this->loadBalancer instanceof LoadBalancerInterface) {
            return $this->loadBalancer->select();
        }
        return $this->nodes[array_rand($this->nodes)];
    }
}