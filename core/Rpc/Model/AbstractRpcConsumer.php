<?php

namespace Whyperf\Rpc\Model;

use App\JsonRpc\Demo\Server\DemoMSServer;
use Hyperf\Amqp\Builder\ExchangeBuilder;
use Hyperf\Amqp\Builder\QueueBuilder;
use Hyperf\Amqp\Message\ConsumerMessage;
use Hyperf\Amqp\Result;
use Hyperf\Testing\Client;
use PhpAmqpLib\Message\AMQPMessage;
use Whyperf\Rpc\Annotation\AbstractMicroService;
use Whyperf\Rpc\QueueServers;
use Whyperf\System\CoroutineEnv\CoreGo;
use Whyperf\Whyperf;

/**
 * Class AbstractRpcClient
 * @package whyperf\Rpc\Model
 * @author WANG RUNXIN
 */
abstract class AbstractRpcConsumer extends ConsumerMessage
{

    const SERVICE_CLASSNAME = "service_classname";

    /**
     * @var Client
     */
    protected $client;

    protected $data;

    protected AbstractMicroService $service;

    function __construct()
    {
        $this->service = $this->initMicroService();
        $this->queue = $this->getTargetQueue();
        $this->exchange = $this->getTargetExchange();
        $this->routingKey = $this->getTargetRoutingkey();
    }

    abstract function initMicroService(): AbstractMicroService;

    protected function getTargetMicroService(): AbstractMicroService
    {
        return $this->service;
    }

    function getTargetQueue(): string
    {
        return $this->getTargetMicroService()->getQueue();
    }

    function getTargetExchange(): string
    {
        return $this->getTargetMicroService()->getExchange();
    }

    function getTargetRoutingkey(): string
    {
        return $this->getTargetMicroService()->getRoutingKey("*");
    }

    public function getExchangeBuilder(): ExchangeBuilder
    {
        return parent::getExchangeBuilder()
            ->setDurable(false)
            ->setAutoDelete(true);
    }

    public function getQueueBuilder(): QueueBuilder
    {
        return parent::getQueueBuilder()
            ->setDurable(false)
            ->setAutoDelete(true)
            ->setExclusive(false);
    }

    public function consumeMessage($data, AMQPMessage $message): string
    {
        if (!$this->isRpcRequest($data)) {
            $this->reply("ignore", $message);
            return Result::ACK;
        }

        try {
            /**
             * @var DemoMSServer $server
             */
            $server = Whyperf::getInstance()->getWhyperfContainer()->get($this->getServiceName($data));

            $request = new \Whyperf\Http\Request("post", "/", ["content-type" => "application/json", "content-length" => strlen($data)], $data);

            $server->disableWaiting()->onRequest($request, null);

            $this->reply(CoreGo::getCoreGo()->getResponse()->getBody()->getContents(), $message);

        } catch (\Throwable $e) {
            return Result::DROP;
        }

        return Result::ACK;
    }

    function isRpcRequest($data): bool
    {
        if (is_string($data)) {
            $data = json_decode($data, true);
        }

        if (!is_array($data)) {
            return false;
        }

        if (isset($data["jsonrpc"])) {
            return true;
        }

        return false;
    }

    function isEnable(): bool
    {
        if ($this->getTargetMicroService()::isDisabled()) {
            return false;
        }

        return true;
    }

    function getServiceName($data)
    {
        if (is_string($data)) {
            $data = json_decode($data, true);
        }

        return QueueServers::getInstance()->getServer($data[self::SERVICE_CLASSNAME]);
    }
}
