<?php

namespace Whyperf\Rpc\Model;

use Hyperf\Amqp\Message\DynamicRpcMessage;
use Hyperf\RpcClient\AbstractServiceClient;
use Hyperf\RpcClient\ServiceClient;

/**
 * Class AbstractRpcClient
 * @package whyperf\Rpc\Model
 * @author WANG RUNXIN
 */
class DynamicRpcQueueMessage extends DynamicRpcMessage
{
    function __construct(string $exchange, string $routingKey, $data, $queue)
    {
        $this->queue = $queue;
        return parent::__construct($exchange, $routingKey, $data);
    }
}