<?php

namespace Whyperf\Rpc\Model;

use Hyperf\RpcClient\AbstractServiceClient;
use Psr\Container\ContainerInterface;
use Whyperf\Rpc\Annotation\AbstractMicroService;
use Whyperf\Rpc\Traits\DynamicQueueTrait;

/**
 * Class AbstractRpcClient
 * @package whyperf\Rpc\Model
 * @author WANG RUNXIN
 */
abstract class AbstractRpcClient extends AbstractServiceClient
{

    use DynamicQueueTrait;

    function __construct(ContainerInterface $container)
    {
        $this->protocol = $this->getService()->getProtocol();
        $this->serviceName = $this->getService()->getName();

        parent::__construct($container);
    }

    protected function __generateData(string $methodName, array $params, ?string $id)
    {
        $data = $this->dataFormatter->formatRequest([$this->__generateRpcPath($methodName), $params, $id]);
        return $this->formatData($data, $methodName);
    }

    abstract function getService(): AbstractMicroService;
}