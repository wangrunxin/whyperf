<?php

namespace Whyperf\Rpc\Model;

use Hyperf\RpcClient\AbstractServiceClient;
use Hyperf\RpcClient\ServiceClient;
use Whyperf\Rpc\Traits\DynamicQueueTrait;

/**
 * Class AbstractRpcClient
 * @package whyperf\Rpc\Model
 * @author WANG RUNXIN
 */
class QueueRpcServiceClient extends ServiceClient
{
    use DynamicQueueTrait;

    protected function __generateData(string $methodName, array $params, ?string $id)
    {
        $data = $this->dataFormatter->formatRequest([$this->__generateRpcPath($methodName), $params, $id]);
        return $this->formatData($data, $methodName);
    }

}