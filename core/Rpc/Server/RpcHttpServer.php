<?php

namespace Whyperf\Rpc\Server;

use Hyperf\Contract\ConfigInterface;

class RpcHttpServer extends \Hyperf\JsonRpc\HttpServer{

    const JSON_RPC_MIDDLEWARE = "jsonrpc";

    public function initCoreMiddleware(string $serverName) : void
    {
        parent::initCoreMiddleware($serverName);
        $config = $this->container->get(ConfigInterface::class);
        $this->middlewares = array_merge($config->get('middlewares.' . $serverName, []), $this->middlewares);
    }
}
