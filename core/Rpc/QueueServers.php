<?php

namespace Whyperf\Rpc;

use Whyperf\Whyperf;

/**
 * Class QueueTransporter
 * @package whyperf\Rpc
 * @author WANG RUNXIN
 */
class QueueServers
{
    protected static array $servers = [];

    function addServer($name, $server)
    {
        QueueServers::$servers[$name] = $server;
    }

    function getServer($name)
    {
        return QueueServers::$servers[$name];
    }

    static function getInstance(): QueueServers
    {
        return Whyperf::getContainer()->get(QueueServers::class);
    }
}