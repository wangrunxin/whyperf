<?php

namespace Whyperf\System\Traits;

use Whyperf\System\CoroutineEnv\CoreGo;

trait CoComponentTrait{

    /**
     * @return static
     * @author WANG RUNXIN
     */
    static function getInstance()
    {
        if (is_null(CoreGo::getCoreGo())) {
            return null;
        }
        return CoreGo::getCoreGo()->getComponent(static::class);
    }

    function attachedJsonRpc(&$json){

    }

    function addJsonRpcHeader(string $key, $value, &$json)
    {
        $json['header'][$key] = $value;
    }
}
