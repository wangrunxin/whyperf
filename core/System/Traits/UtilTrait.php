<?php

namespace Whyperf\System\Traits;

use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Logger\LoggerFactory;
use Hyperf\Utils\ApplicationContext;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Psr\Log\LoggerInterface;
use const null;

/**
 * Trait UtilTrait
 * @package whyperf\System\Traits
 */
trait UtilTrait
{

    /**
     *
     * @return string
     */
    static function getRuntimePath(): string
    {
        return BASE_PATH . DIRECTORY_SEPARATOR . "runtime" . DIRECTORY_SEPARATOR;
    }

    /**
     *
     * @return string
     */
    static function getPath(): string
    {
        return __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR;
    }

    /**
     *
     * @return string
     */
    static function getLogPath(): string
    {
        return self::getRuntimePath() . "logs" . DIRECTORY_SEPARATOR;
    }

    static function getConsoleLogger(): StdoutLoggerInterface
    {
        return ApplicationContext::getContainer()->get(StdoutLoggerInterface::class);
    }

    static function getLogger($name = "default"): LoggerInterface
    {
        try {
            /**
             * use ApplicationContext in case of system initialization error.
             */
            return ApplicationContext::getContainer()->get(\Hyperf\Logger\LoggerFactory::class)->get($name);
        } catch (\TypeError $error) {
            /**
             * Framework has not init yet.
             */
            return self::getSimpleLogger($name);
        }
    }

    static function getSimpleLogger($name)
    {
        return (new Logger($name))
            ->pushHandler(new StreamHandler(self::getLogPath() . "system.error.log"), Logger::ERROR);
    }
}

?>