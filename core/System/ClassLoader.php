<?php namespace Whyperf\System;

use Composer\Autoload\ClassLoader as ComposerClassLoader;
use Hyperf\Di\Annotation\ScanConfig;
use Hyperf\Di\Annotation\Scanner;
use Hyperf\Di\Aop\ProxyManager;

class ClassLoader extends \Hyperf\Di\ClassLoader{
}