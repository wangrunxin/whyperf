<?php

namespace Whyperf\System;

use App\App;
use Hyperf\Amqp\Message\DynamicRpcMessage;
use Hyperf\Amqp\Message\ProducerMessageInterface;
use Hyperf\DbConnection\Pool\PoolFactory;
use Hyperf\Dispatcher\AbstractDispatcher;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Exception\Handler\HttpExceptionHandler;
use Hyperf\JsonRpc\JsonRpcTransporter;
use Hyperf\Logger\Logger;
use Hyperf\ModelCache\Manager;
use Lcobucci\JWT\Configuration;
use Psr\EventDispatcher\EventDispatcherInterface;
use Richard\HyperfPassport\AuthorizationServerFactory;
use Richard\HyperfPassport\ConfigFactory;
use Whyperf\Authenticator\AbstractDefender;
use Whyperf\Authenticator\Superman;
use Whyperf\Authenticator\Surrender;
use Whyperf\Exception\Handler\AppExceptionHandler;
use Whyperf\Http\HttpRequest;
use Whyperf\Log\ZipkinLogger;
use Whyperf\Model\Traits\Singleton;
use Whyperf\MultiTenant\AbstractTenantFinder;
use Whyperf\MultiTenant\DbConnection\CacheManager;
use Whyperf\MultiTenant\StaticTenantFinder;
use Whyperf\Rpc\ClientInterfaceDi;
use Whyperf\Rpc\QueueServers;
use Whyperf\Rpc\ServiceServer;
use Whyperf\System\CoroutineEnv\CoreGo;
use Whyperf\System\CoroutineEnv\CoreGoFactory;
use Whyperf\Tcc\TccMessageProducer;
use Whyperf\Whyperf;

/**
 * Class AbstractSystemConfig
 * @package whyperf\System
 * @author WANG RUNXIN
 */
class AbstractSystemConfig
{

    use Singleton;

    /**
     * @var bool
     */
    private $enableAudit = false;

    function __construct()
    {
        $this->init();
    }

    private function init()
    {
        Whyperf::logo();
        $this->microServiceClientDi();
        $this->coreGoComponentsDi();
        $this->extDi();
    }

    protected function microServiceClientDi()
    {
        ClientInterfaceDi::getInstance();
    }

    function extDi()
    {

    }

    function getLazyCoreGoComponents(): array
    {
        return [];
    }

    function coreGoComponentsDi()
    {
        Whyperf::getContainer()->define(AbstractDefender::class, Surrender::class);
        Whyperf::getContainer()->define(AbstractTenantFinder::class, StaticTenantFinder::class);
        Whyperf::getContainer()->define(QueueServers::class, QueueServers::class);
    }

    function getCoreGoComponents(): array
    {

        return [];
    }

    static function getServiceList(): array
    {
        return [];
    }

    static function getRpcDir(): array
    {
        return [
            BASE_PATH . DIRECTORY_SEPARATOR . "app" . DIRECTORY_SEPARATOR . "JsonRpc"
        ];
    }

    static function getServiceDir(): array
    {
        return [
            BASE_PATH . DIRECTORY_SEPARATOR . "app" . DIRECTORY_SEPARATOR . "Service"
        ];
    }

    function setTimeZone()
    {
        date_default_timezone_set("Asia/Hong_Kong");
    }

    /**
     * rate = given value of 100
     * @return int
     */
    function getAuditSamplingRate(): int
    {
        return 5;
    }

    static function isLazyMakeClass(object $object): bool
    {
        if (method_exists($object, "isLazyMakeClass")) {
            return $object->isLazyMakeClass();
        }

        return false;
    }

    /**
     * @return bool
     */
    function dumpLinstenerInfo(): bool
    {
        return false;
    }

    static function getName(): string
    {
        return "whyperf";
    }

    function getTccExchange(): string
    {
        return sprintf("whyperf-%s-tcc-exchange", App::getName());
    }

    function getTccRoutingKey(): string
    {
        return sprintf("whyperf-%s-tcc-routingkey", App::getName());
    }

    function getTccTopic(): string
    {
        return sprintf("whyperf-%s-tcc-topic", App::getName());
    }

    function getTccQueue(): string
    {
        return sprintf("whyperf-%s-tcc-queue", App::getName());
    }

    function enableTcc(): bool
    {
        return false;
    }
}
