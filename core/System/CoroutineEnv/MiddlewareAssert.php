<?php

namespace Whyperf\System\CoroutineEnv;

use phpDocumentor\Reflection\Types\Mixed_;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Whyperf\Authenticator\AbstractDefender;
use Whyperf\System\Traits\CoComponentTrait;
use Whyperf\Whyperf;

/**
 * Class MiddlewareAssert
 * @package Whyperf\System\CoroutineEnv
 * @author WANG RUNXIN
 */
abstract class MiddlewareAssert implements CoComponent
{
    use CoComponentTrait;
    /**
     * @var ServerRequestInterface
     */
    private ServerRequestInterface $request;

    /**
     * @var bool
     */
    private bool $pass = true;

    /**
     * @var ResponseInterface
     */
    private ResponseInterface $response;

    protected $assert;

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    static function auth(ServerRequestInterface $request, $callback)
    {
        if (is_null(self::getInstance())) {
            return (new static())->setRequest($request)->process($callback);
        }

        return $callback($request);
    }

    static function preload($request)
    {
        if (!is_null(self::getInstance())) {
            return true;
        }

        return (new static())->setRequest($request)->process();
    }

    abstract function getAssertClass(): string;

    /**
     * @param ServerRequestInterface $request
     * @return $this
     */
    function setRequest(ServerRequestInterface $request): self
    {
        $this->request = $request;
        return $this;
    }

    function getRequest(): ServerRequestInterface
    {
        return $this->request;
    }

    /**
     * @param AbstractCoreGoAssert $assert
     * @return $this
     */
    function setAssert(AbstractCoreGoAssert $assert): self{
        $this->assert = $assert;
        return $this;
    }

    function getAssert(): AbstractCoreGoAssert
    {
        if(is_null($this->assert)){
            $this->assert = Whyperf::getContainer()->make($this->getAssertClass());
        }

        return $this->assert;
    }

    function getResponse(): ResponseInterface
    {
        return $this->response;
    }

    function setFalseResult(ResponseInterface $response)
    {
        $this->pass = false;
        $this->response = $response;
        CoreGo::getCoreGo()->setResponse($response);
    }

    function isPassed(): bool
    {
        return $this->pass;
    }

    function process(callable $callback = null)
    {
        $this->getAssert()->run($this);

        if ($this->pass) {
            CoreGo::getCoreGo()->addComponent($this);
            if (!is_null($callback)) {
                return $callback($this->getRequest());
            }
            return true;
        }

        if (!is_null($callback)) {
            return $this->getResponse();
        }
        return false;
    }
}
