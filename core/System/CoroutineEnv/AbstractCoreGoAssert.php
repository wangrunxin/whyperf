<?php

namespace Whyperf\System\CoroutineEnv;

use Hyperf\HttpMessage\Base\Response;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\Utils\Context;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Whyperf\Authenticator\AbstractDefender;
use Whyperf\Whyperf;

abstract class AbstractCoreGoAssert
{
    function run(MiddlewareAssert $middlewareAssert)
    {
        /** @var ResponseInterface $response */
        $response = Context::get(ResponseInterface::class)
            ->withStatus(self::STATUS_UNAUTHORIZED)
            ->withBody(
                new SwooleStream(
                    json_encode([
                        "code" => self::STATUS_UNAUTHORIZED,
                        "result" => Response::getReasonPhraseByCode(self::STATUS_UNAUTHORIZED),
                        "message" => "AbstractCoreGoAssert->run() must be implemented."
                    ])
                )
            );

        $middlewareAssert->setFalseResult($response);
    }
}