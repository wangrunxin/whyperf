<?php

namespace Whyperf\System\CoroutineEnv;

/**
 * Interface CoComponentInterface
 * @package Whyperf\System\CoroutineEnv
 * @author WANG RUNXIN
 */
interface CoComponent
{

    /**
     * @return static
     * @author WANG RUNXIN
     */
    static function getInstance();

    function attachedJsonRpc(&$json);

    function addJsonRpcHeader(string $key, $value, &$json);
}