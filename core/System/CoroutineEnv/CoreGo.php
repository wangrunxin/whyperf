<?php

namespace Whyperf\System\CoroutineEnv;

use App\App;
use Hyperf\Di\Aop\PropertyHandlerTrait;
use Jaeger\Span;
use Hyperf\HttpServer\Request;
use Psr\Http\Message\ResponseInterface;
use Hyperf\Utils\Context;
use Hyperf\Utils\Coroutine;
use Hyperf\Utils\WaitGroup;
use Psr\Http\Message\ServerRequestInterface;
use Whyperf\Audit\Audit;
use Whyperf\Model\Traits\CoroutineSafeSingleton;
use Whyperf\Whyperf;

/**
 * Class CoreGo
 * @package whyperf\System
 * @author WANG RUNXIN
 */
class CoreGo
{
    use PropertyHandlerTrait;
    use CoroutineSafeSingleton;

    const REQUEST_KEY = "requestKey";

    private $key;

    private $coreGo;

    private $components = [];

    private $isCore = false;

    private $startAt = 0;

    private $response;

    private $request;

    protected $trace;

    protected $span;

    function __construct()
    {
        $this->loadComponents();
    }

    function setMainCore(): CoreGo
    {
        $this->coreGo = $this;
        $this->isCore = true;
        $this->response = Context::get(ResponseInterface::class);
        $this->request = Context::get(ServerRequestInterface::class);
        $this->initKey();
        return $this;
    }

    function initTracerRoot(){
        $this->trace = Context::get('tracer.root');
    }

    /**
     * @return $this
     * @author WANG RUNXIN
     */
    function setStartAt(): self
    {
        $this->startAt = microtime(true);
        return $this;
    }

    function setSpan(Span $span): CoreGo
    {
        if($this->isCore){
            $this->span = $span;
            return $this;
        }

        return $this->getCore()->setSpan($span);
    }

    function getSpan(): ?Span{
        if($this->isCore){
            return $this->span;
        }

        return $this->getCore()->getSpan();
    }

    /**
     * @return int
     */
    function getStartAt(): float
    {
        return $this->startAt;
    }

    protected function loadKey()
    {
        if (is_null(CoreGo::getCoreGo())) {
            return $this->initKey();
        }

        $this->key = CoreGo::getCoreGo()->getKey();
    }

    protected function initKey()
    {
        $header = [];
        $request = Context::get(ServerRequestInterface::class);
        if (!is_null($request)) {
            $header = $request->getHeader(self::REQUEST_KEY);
        }

        $this->key = $header[0] ?? uniqid("core.go.");
    }

    function getKey()
    {
        if(is_null($this->key)){
            $this->initKey();
        }
        return $this->key;
    }

    function setKey($key)
    {
        $this->key = $key;
    }

    function setCore(CoreGo $coreGo)
    {
        $this->coreGo = $coreGo;
        $this->initCore();
    }

    function loadTrace(): CoreGo
    {
        if ($this->isCore) {
            $this->trace = Context::get('tracer.root');
            return $this;
        }

        return $this->getCore()->loadTrace();
    }

    function getTrace()
    {
        return $this->trace;
    }

    protected function initCore()
    {
        Context::set('tracer.root', $this->getCore()->getTrace());
        Context::set(ResponseInterface::class, $this->getCore()->getResponse());
        Context::set(ServerRequestInterface::class, $this->getCore()->getRequest());
        $this->loadKey();
    }

    /**
     * @param $key
     * @return false|mixed
     */
    function getComponent($key)
    {
        return $this->findComponent($key);
    }

    /**
     * @return CoComponent[]
     */
    function getComponents()
    {
        if (!$this->isCore) {
            CoreGo::getCoreGo()->getComponents();
        }

        return $this->components;
    }

    /**
     * @param $key
     * @return false|mixed
     */
    function removeComponent($key)
    {
        if (!$this->isCore) {
            CoreGo::getCoreGo()->removeComponent($key);
        }

        unset($this->components[$key]);
    }

    /**
     * @param CoComponent $coComponent
     * @param string|null $key
     * @return $this
     * @author WANG RUNXIN
     */
    function addComponent(CoComponent $coComponent, string $key = null): self
    {
        if (!$this->isCore) {
            CoreGo::getCoreGo()->addComponent($coComponent);
        }

        if(is_null($key)){
            $key = get_class($coComponent);
        }

        $this->components[$key] = $coComponent;
        return $this;
    }

    /**
     * @param $key
     * @return false|mixed
     */
    private function findComponent($key)
    {
        if ($this->isCore) {
            if (isset($this->components[$key])) {
                return $this->components[$key];
            }

            return null;
        }

        return $this->getCore()->getComponent($key);
    }

    private function loadComponents()
    {
        $components = App::getInstance()->getCoreGoComponents();
        foreach ($components as $key => $class) {
            $this->components[$class] = Whyperf::getContainer()->make($class);
        }
    }

    /**
     * @return CoreGo|null
     * @author WANG RUNXIN
     */
    private function getCore()
    {
        return $this->coreGo;
    }

    /**
     * @return CoreGo|null
     * @author WANG RUNXIN
     */
    static function getCoreGo()
    {
        return CoreGo::getInstance()->getCore();
    }

    /**
     * @param ResponseInterface $response
     */
    function setResponse(ResponseInterface &$response)
    {
        if (!$this->coreGo) {
            $this->getCore()->setResponse($response);
        }

        $this->response = $response;
    }

    /**
     * @return ResponseInterface|null
     */
    function getResponse()
    {
        if (!$this->coreGo) {
            $this->getCore()->getResponse();
        }

        if (is_null($this->response)) {
            $this->response = Context::get(ResponseInterface::class);
        }

        return $this->response;
    }

    /**
     * @param ServerRequestInterface $response
     */
    function setRequest(ServerRequestInterface &$request): self
    {
        if (!$this->coreGo) {
            $this->getCore()->setRequest($request);
        }

        $this->request = $request;

        return $this;
    }

    /**
     * @return ServerRequestInterface|null
     */
    function getRequest()
    {
        if (!$this->coreGo) {
            $this->getCore()->getRequest();
        }

        return $this->request;
    }

    function before_destroy()
    {
        if ($this->destroyed) {
            return;
        }

        $this->destroyed = true;

        if (!$this->isCore) {
            return;
        }

        $this->mainCoroutineExit();
    }

    private function mainCoroutineExit()
    {
        //TODO::addsomething
    }

    /**
     * @return static
     */
    static function prepareEnv(): self{
        if (is_null(CoreGo::getCoreGo())) {
            CoreGo::getInstance()->setMainCore()->setStartAt();
        }

        return CoreGo::getCoreGo();
    }

    static function Run(callable $func, bool $wait = false, CoreGo $core = null)
    {
        if (is_null($core)) {
            $core = CoreGo::getInstance();
        }

        $waitGroup = null;

        if ($wait) {
            $waitGroup = new WaitGroup();
            $waitGroup->add();
        }

        $exception = null;
        co(function () use ($func, &$core, $waitGroup, &$exception) {
            try {
                /**
                 * @var CoreGo $core
                 */
                $coreGo = CoreGo::getInstance();
                $coreGo->setCore($core->getCore());
                $func();
            } catch (\Throwable $throwable) {
                $exception = $throwable;
                if (is_null($waitGroup)) {
                    throw $exception;
                }
            } finally {
                if (!is_null($waitGroup)) {
                    $waitGroup->done();
                }
            }
        });

        if (!is_null($waitGroup)) {
            $waitGroup->wait();
        }

        if (!is_null($exception) && $exception instanceof \Throwable) {
            throw $exception;
        }
    }
}