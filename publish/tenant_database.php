<?php

return [
    "pool" => [
        'min_connections' => 1,
        'max_connections' => 10,
        'connect_timeout' => 10.0,
        'wait_timeout' => 3.0,
        'heartbeat' => -1,
        'max_idle_time' => (float)env('DB_MAX_IDLE_TIME', 60),
    ],
    "debug" => false,
    "include_trace" => false,
];